﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CrickBd.AutoMapper;
using CrickBd.Model;
using CrickBd.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Logging;

namespace CrickBd.Controllers
{
    [ApiController]
    [Route("weather/{id:int}")]
    public class WeatherForecastController : ControllerBase
    {

        private readonly IActionDescriptorCollectionProvider _actionDescriptorCollectionProvider;

        
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IMapper _mapper;
        private readonly IMatchService _matchService;

        public WeatherForecastController(ILogger<WeatherForecastController> logger,
            IMapper mapper,
            IMatchService matchService,
            IActionDescriptorCollectionProvider actionDescriptorCollectionProvider
            )
        {
            _logger = logger;
            _mapper = mapper;
            _matchService = matchService;
            _actionDescriptorCollectionProvider = actionDescriptorCollectionProvider;
        }

        [HttpGet]
        [Route("information")]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
        [HttpPost]
        [Route("run")]
        public async Task<ActionResult<string>> Run(Batting contact)  
        {
            return Ok(contact);
        }
        [Route("routes")]
        public async Task<IActionResult> GetAllRoutes()
        {
            var routes = _actionDescriptorCollectionProvider.ActionDescriptors.Items.Where(
                ad => ad.AttributeRouteInfo != null).Select(ad => new RouteModel
                {
                    Name = ad.AttributeRouteInfo.Name,
                    Template = ad.AttributeRouteInfo.Template
                }).ToList();
            
            return Ok(routes);
        }

    }
}
