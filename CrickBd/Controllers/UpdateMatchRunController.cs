﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CrickBd.Domain;
using CrickBd.Model;
using CrickBd.Model.MatchModel;
using CrickBd.Service;
using CrickBd.Team;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CrickBd.Controllers
{
    [Route("api/match")]
    [ApiController]
    public class UpdateMatchRunController : ControllerBase
    {
        private readonly IMatchService matchService;

        public UpdateMatchRunController
            (
                IMatchService matchService
            )
        {
            this.matchService = matchService;
        }

        [Route("team/get/score")]
        public async Task<IActionResult> GetScoreByMatchId(int matchId , int teamType)
        {
            try
            {
                ITeam team = TeamCreate.GetTeam(teamType, matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                int totalRun = await teamFactory.GetTeamScore();
                return Ok(totalRun);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [Route("team/update/score")]
        public async Task<IActionResult> UpdateTeamScore(int matchId , int teamType , int run , int ballStatus)
        {
            try
            {
                ITeam team = TeamCreate.GetTeam(teamType, matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                int totalRun = await teamFactory.UpdateTeamScore(run, ballStatus);
                return Ok(totalRun);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
