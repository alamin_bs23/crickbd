﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CrickBd.AutoMapper;
using CrickBd.Model;
using CrickBd.Service;
using CrickBd.Service.Player;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CrickBd.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IMapper mapper;
        private readonly IPlayerService playerService;

        public UserController(
            IUserService userService,
            IMapper mapper,
            IPlayerService playerService
            )
        {
            this.userService = userService;
            this.mapper = mapper;
            this.playerService = playerService;
        }
        [HttpPost("authenticate")]
        public IActionResult Authenticate(AuthenticateRequest model)
        {
            var response = userService.AuthenticateAsync(model);
            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });
            return Ok(response);
        }
        [HttpPost("signup")]
        public async Task<IActionResult> Signup(Model.Player model)
        {
            var player = mapper.Map<Domain.Player>(model);
            player = await playerService.RegistrationPlayer(player);
            return Ok(player);
        }
        [HttpGet]
        [Route("all")]
        public async Task<IActionResult> GetAllByUserName(string userName)
        {
            var users = await userService.GetAllByUserNameAsync(userName);
            return Ok(users.Take(10)); 
        }
        [HttpGet]
        [Route("exist")]
        public async Task<IActionResult> IsUserExistByUserName(string userName)
        {
            try
            {
                var user = await userService.GetByUserNameAsync(userName);
                var isUserExist = user == null ? false : true;
                return Ok(isUserExist);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
