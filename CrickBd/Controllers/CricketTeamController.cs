﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CrickBd.Domain;
using CrickBd.Model;
using CrickBd.Model.MatchModel;
using CrickBd.Service;
using CrickBd.Team;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CrickBd.Controllers
{
    [Route("api/team")]
    [ApiController]
    public class CricketTeamController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IMatchService matchService;

        public CricketTeamController(
            IMapper mapper,
            IMatchService matchService
            )
        {
            this.mapper = mapper;
            this.matchService = matchService;
        }

        [Route("create")]
        public async Task<IActionResult> Create(CricketTeamModel model)
        {
            ITeam team = TeamCreate.GetTeam(model.TeamType, model.matchId);
            TeamFactory teamFactory = new TeamFactory(team);
            var cricketTeam = mapper.Map<CricketTeam>(model);
            await teamFactory.CreateTeamAsync(cricketTeam);
            return Ok(cricketTeam);
        }
        [Route("create/add/member")]
        public async Task<IActionResult> AddTeamMember(TeamCreationModel model)
        {
            try
            {
                ITeam team = TeamCreate.GetTeam(model.TeamType, model.matchId); 
                TeamFactory teamFactory = new TeamFactory(team); 
                CricketTeam cricketTeam = mapper.Map<CricketTeam>(model); 
                await teamFactory.CreateTeamAsync(cricketTeam); 
                int addedMember = await teamFactory.InsertTeamMemberByGroupAsync(model); 
                return Ok(new { memberAdded = addedMember }); 
            }
            catch (Exception ex) 
            {
                throw new Exception(ex.Message); 
            }
        }
        [Route("existing/member")]
        public async Task<IActionResult> IsExistAllUserName(TeamCreationModel model)
        {
            try
            {
                ITeam team = TeamCreate.GetTeam(model.TeamType, model.matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                int existingPlayer = await teamFactory.IsExistAllMemberByUserNameAsync(model);
                return Ok(existingPlayer);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [Route("isCreated")]
        public async Task<IActionResult> IsTeamCreatedByTeamType(int teamType , int matchId)
        {
            try
            {
                ITeam team = TeamCreate.GetTeam(teamType, matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                bool isTeamCreated = await teamFactory.IsTeamCreatedAsync();
                return Ok(isTeamCreated);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [Route("get/members")]
        public async Task<IActionResult> GetAllTeamMembers(int teamType , int matchId)
        {
            try
            {
                ITeam team = TeamCreate.GetTeam(teamType, matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                List<Domain.Player> players = await teamFactory.GetTeamMembers() as List<Domain.Player>;
                return Ok(players);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        

        [Route("get/non/stricker")]
        public async Task<IActionResult> GetNonStricker(int teamType , int matchId)
        {
            try
            {
                ITeam team = TeamCreate.GetTeam(teamType, matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                PlayerInMatch playerInMatch = await teamFactory.GetNonStricker();
                return Ok(playerInMatch);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [Route("get/on/stricker")]
        public async Task<IActionResult> GetOnStricker(int teamType , int matchId)
        {
            try
            {
                ITeam team = TeamCreate.GetTeam(teamType, matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                PlayerInMatch playerInMatch = await teamFactory.GetOnStricker();
                return Ok(playerInMatch);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [Route("get/current/bowler")]
        public async Task<IActionResult> GetCurrentBowler(int teamType , int matchId)
        {
            try
            {
                ITeam team = TeamCreate.GetTeam(teamType, matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                PlayerInMatch playerInMatch = await teamFactory.GetCurrentBowler();
                return Ok(playerInMatch);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [Route("batsman/isselected")]
        public async Task<IActionResult> IsBatsmanIsSelected(int matchId)
        {
            try
            {
                TeamGroup teamGroup = (TeamGroup)await this.matchService.GetBattingTeamAsync(matchId);
                ITeam team = TeamCreate.GetTeam((int)teamGroup, matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                PlayerInMatch playerInMatch = await teamFactory.GetOnStricker();
                var result = playerInMatch != null ? true : false;
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [Route("bowler/isselected")]
        public async Task<IActionResult> IsBowlerIsSelected(int matchId)
        {
            try
            {
                TeamGroup teamGroup = (TeamGroup)await this.matchService.GetBattingTeamAsync(matchId);
                teamGroup = SwapEnum.swapTeamGroup(teamGroup);
                ITeam team = TeamCreate.GetTeam((int)teamGroup, matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                PlayerInMatch playerInMatch = await teamFactory.GetCurrentBowler();
                var result = playerInMatch != null ? true : false;
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [Route("get/recent/ball")]
        public async Task<IActionResult> GetRecentBalls(int matchId)
        {
            try
            {
                TeamGroup teamGroup = (TeamGroup)await this.matchService.GetBattingTeamAsync(matchId);
                ITeam team = TeamCreate.GetTeam((int)teamGroup, matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                IList<TeamBallsRecords> teamBallsRecords = await teamFactory.GetTeamBallsRecords();
                return Ok(teamBallsRecords);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [Route("swap/stricker/nonstricker")]
        public async Task<IActionResult> SwapStrickerNonStricker(int matchId)
        {
            try
            {
                TeamGroup teamGroup = (TeamGroup)await this.matchService.GetBattingTeamAsync(matchId);
                ITeam team = TeamCreate.GetTeam((int)teamGroup, matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                bool result = await teamFactory.SwapOnStrickerAndNonStricker();
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [Route("set/bowler")]
        public async Task<IActionResult> SetBowlerInTheBowlingTeam(int matchId , int playerId)
        {
            try
            {
                TeamGroup teamGroup = (TeamGroup)await this.matchService.GetBattingTeamAsync(matchId);
                ITeam team = TeamCreate.GetTeam((int)teamGroup, matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                var result = await teamFactory.SetBowlerInBowlingTeam(playerId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [Route("set/batsman")]
        public async Task<IActionResult> SetBatsmanInTheBattingTeam(int matchId , int playerId)
        {
            try
            {
                TeamGroup teamGroup = (TeamGroup)await this.matchService.GetBattingTeamAsync(matchId);
                ITeam team = TeamCreate.GetTeam((int)teamGroup, matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                var result = await teamFactory.SetBatsManInBattingTeam(playerId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [Route("out/batsman")]
        public async Task<IActionResult> OutBatsmanInTheBattingTeam(int matchId , int playerId)
        {
            try
            {
                TeamGroup teamGroup = (TeamGroup)await this.matchService.GetBattingTeamAsync(matchId);
                ITeam team = TeamCreate.GetTeam((int)teamGroup, matchId);
                TeamFactory teamFactory = new TeamFactory(team);
                var result = await teamFactory.OutBatsManInBattingTeam(playerId);
                return Ok(result);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
