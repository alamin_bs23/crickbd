﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrickBd.AutoMapper;
using CrickBd.CricketMatch;
using CrickBd.Model.MatchModel;
using CrickBd.Service;
using CrickBd.Team;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CrickBd.Controllers
{
    [Route("api/cricket/match")]
    [ApiController]
    public class BdCricketMatchController : ControllerBase
    {
        private readonly IMatchService matchService;

        public BdCricketMatchController(IMatchService matchService)
        {
            this.matchService = matchService;
        }
        [HttpPost]
        [Route("create")]
        public async Task<ActionResult<Domain.Match>> Create(Model.Match match)
        {
            var config = new AutoMapperConfiguration().Configure();
            var iMapper = config.CreateMapper();
            var matchCreate = iMapper.Map<Model.Match, Domain.Match>(match);
            await matchService.InsertMatchAsync(matchCreate);
            var team = new TeamA(matchCreate.Id);
            return Ok(matchCreate); 
        }
        [HttpGet]
        [Route("get/matches")]
        public async Task<ActionResult<Domain.Match>> GetMatches(int userId)
        {
            try
            {
                List<Domain.Match> matches = new List<Domain.Match>();
                await Task.Run(() =>
                {
                    matches = matchService.GetAllMatches().ToList();
                });
                return Ok(matches);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpGet]
        [Route("player/count")]
        public async Task<IActionResult> GetMatchPlayerCount(int matchId , int userId)
        {
            try
            {
                ICricketMatch cricketMatch = MatchFactory.GetMatch(matchId, userId);
                int playerCount = await cricketMatch.GetNumberOfPlayerInMatchByMatchIdAndUserId();
                return Ok(playerCount);
            }
            catch (Exception e)
            {
                throw new NullReferenceException(e.Message);
            }
        }
        [HttpPost]        
        [Route("toss/winner")]
        public async Task<IActionResult> UpdateTossWinner(TossModel model)
        {
            try
            {
                ICricketMatch cricketMatch = MatchFactory.GetMatch(model.matchId, model.userId);
                await cricketMatch.UpdateTossWinner(model);
                return Ok(true);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [HttpGet]
        [Route("get/batting/team")]
        public async Task<IActionResult> GetBattingTeam(int matchId)
        {
            try
            {
                TeamGroup teamGroup = (TeamGroup) await this.matchService.GetBattingTeamAsync(matchId);
                return Ok(teamGroup);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        [Route("add/batsman/nonstricke")]
        public async Task<IActionResult> AddBatsmanAsNonStrike(PlayerIdsModel model)
        {
            try
            {
                ICricketMatch cricketMatch = MatchFactory.GetMatch(model.matchId, model.userId);
                bool isSuccess =  await cricketMatch.UpdateNonStricker(model);
                return Ok(isSuccess);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [Route("add/batsman/onstricke")]
        public async Task<IActionResult> UpdateBatsmanAsOnStricke(PlayerIdsModel model)
        {
            try
            {
                ICricketMatch cricketMatch = MatchFactory.GetMatch(model.matchId, model.userId);
                bool isSuccess = await cricketMatch.UpdateOnStricker(model);
                return Ok(isSuccess);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [Route("update/batsman/out")]
        public async Task<IActionResult> UpdateBatsmanAsOut(PlayerIdsModel model)
        {
            try
            { 
                ICricketMatch cricketMatch = MatchFactory.GetMatch(model.matchId, model.userId);
                bool isSuccess = await cricketMatch.UpdatePlayerIsOut(model);
                return Ok(isSuccess);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [Route("update/current/bowler")]
        public async Task<IActionResult> UpdateAsCurrentBowler(PlayerIdsModel model)
        {
            try
            { 
                ICricketMatch cricketMatch = MatchFactory.GetMatch(model.matchId, model.userId);
                bool isSuccess = await cricketMatch.UpdateAsCurrentBowler(model);
                return Ok(isSuccess);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        


        
    }
}
