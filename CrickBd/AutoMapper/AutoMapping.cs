﻿using AutoMapper;
using CrickBd.Domain;
using CrickBd.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.AutoMapper
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<Model.Match, Domain.Match>();
        }
    }
}
