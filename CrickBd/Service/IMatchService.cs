﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrickBd.Domain;
using CrickBd.Model.MatchModel;

namespace CrickBd.Service
{
    public interface IMatchService
    {
        public Task<Domain.Match> InsertMatchAsync(Domain.Match match);
        public Task<Domain.Match> GetMatchByIdAsync(int id);
        public IList<Domain.Match> GetMatchByUserId(int userId);
        Task DeletMatchAsync(int macthId);
        public IList<Domain.Match> GetAllMatches();
        Task<int> GetNumberOfPlayerInMatch(int matchId, int userId);
        Task UpdateTossWinner(TossModel tossModel);
        Task<int> GetBattingTeamAsync(int matchId);
        Task<int> GetBowlingTeamTypeAsync(int matchId); 
        Task<PlayerInMatch> GetPlayerInMatchByMatchIdAndPlayerId(int matchId, int playerId);
        Task<PlayerInMatch> GetNonStrickerByMatchIdAndCricketTeamId(int matchId, int cricketTeamId); 
        Task<PlayerInMatch> GetOnStrickerByMatchIdAndCricketTeamId(int matchId, int cricketTeamId); 
        Task<PlayerInMatch> GetCurrentBowlerByMatchIdAndCricketTeamId(int matchId, int cricketTeamId);
        Task<bool> SwapOnStrickerAndNonStricker(PlayerInMatch onStricker, PlayerInMatch nonStricker);
        Task<bool> SetBowlerInBallingTeam(int matchId, int playerId);
        Task<bool> SetBatsmanInBattingTeam(int matchId, int playerId);
        Task<bool> OutBatsManInBattingTeam(int matchId, int playerId);
        void ApplyChanges();
    }
}
