﻿using CrickBd.Data;
using CrickBd.Domain;
using CrickBd.Model;
using CrickBd.Model.MatchModel;
using CrickBd.Service.CricketsTeam;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Service
{
    public class MatchService : IMatchService
    {
        private readonly CricketDbContext context;
        private readonly ICricketTeamService cricketTeamService;

        public MatchService(
            CricketDbContext context,
            ICricketTeamService cricketTeamService
            )
        {
            this.context = context;
            this.cricketTeamService = cricketTeamService;
        }

        public async Task DeletMatchAsync(int macthId)
        {
            try
            {
                Domain.Match match = await GetMatchByIdAsync(macthId);
                context.Matchs.Remove(match);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<Domain.Match> GetAllMatches()
        {
            try
            {
                List<Domain.Match> matches = context.Matchs.ToList();
                return matches;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Task<int> GetBattingTeamAsync(int matchId)
        {
            try
            {
                Domain.Match match = context.Matchs.Where(x => x.Id == matchId).FirstOrDefault();
                return Task.FromResult(match.WhichGroupBattingFirst);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Domain.Match> GetMatchByIdAsync(int id)
        {
            try
            {
                Domain.Match match = await context.Matchs.FindAsync(id);
                return match;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public IList<Domain.Match> GetMatchByUserId(int userId)
        {
            try
            {
                List<Domain.Match> matches = context.Matchs.Where(x => x.CreatedBy == userId).ToList();
                return matches;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Task<int> GetNumberOfPlayerInMatch(int matchId, int userId)
        {
            Domain.Match match = context.Matchs.SingleOrDefault(x => x.Id == matchId && x.CreatedBy == userId);
            return Task.FromResult(match.NumberOfPlayer);
        }

        public Task<PlayerInMatch> GetNonStrickerByMatchIdAndCricketTeamId(int matchId, int cricketTeamId)
        {
            PlayerInMatch playerInMatch = context.playerInMatches.Where(x => x.MatchId == matchId && x.CricketTeamId == cricketTeamId && x.IsNonStrick == true).FirstOrDefault();
            return Task.FromResult(playerInMatch);
        }

        public Task<PlayerInMatch> GetPlayerInMatchByMatchIdAndPlayerId(int matchId, int playerId)
        {
            PlayerInMatch playerInMatch = context.playerInMatches.Where(x => x.MatchId == matchId && x.PlayerId == playerId).FirstOrDefault();
            return Task.FromResult(playerInMatch);
        }

        public async Task<Domain.Match> InsertMatchAsync(Domain.Match match)
        {
            if (match == null)
                return null;
            await context.Matchs.AddAsync(match);
            await context.SaveChangesAsync();
            return match;
        }

        public async Task UpdateTossWinner(TossModel tossModel)
        {
            try
            {
                if(tossModel == null)
                {
                    throw new NullReferenceException(nameof(tossModel));
                }

                Domain.Match match = await GetMatchByIdAsync(tossModel.matchId);
                
                if(match == null)
                {
                    throw new NullReferenceException(nameof(match));
                }

                match.WhichGroupBattingFirst = tossModel.battingFirstTeam;
                match.TossWinnerGroup = tossModel.winnerTeam;
                context.Matchs.Update(match);
                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Task<PlayerInMatch> GetOnStrickerByMatchIdAndCricketTeamId(int matchId, int cricketTeamId)
        {
            PlayerInMatch playerInMatch = context.playerInMatches.Where(x => x.MatchId == matchId && x.CricketTeamId == cricketTeamId && x.IsOnStrick == true).FirstOrDefault();
            return Task.FromResult(playerInMatch);
        }

        public Task<PlayerInMatch> GetCurrentBowlerByMatchIdAndCricketTeamId(int matchId, int cricketTeamId)
        {
            PlayerInMatch playerInMatch = context.playerInMatches.Where(x => x.MatchId == matchId && x.CricketTeamId == cricketTeamId && x.IsCurrentBowler == true).FirstOrDefault();
            return Task.FromResult(playerInMatch);
        }

        public void ApplyChanges()
        {
            context.SaveChanges();
        }

        public async Task<bool> SwapOnStrickerAndNonStricker(PlayerInMatch onStricker, PlayerInMatch nonStricker)
        {
            try
            {
                int onStrickerId = onStricker.PlayerId,
                    nonStrickerId = nonStricker.PlayerId,
                    matchId = onStricker.MatchId,
                    onStrickerCricketTeamId = onStricker.CricketTeamId,
                    nonStrickerCricketTeamId = nonStricker.CricketTeamId;
                onStricker = await GetPlayerInMatchByMatchIdAndPlayerId(matchId, onStrickerId);
                if (onStricker != null)
                {
                    onStricker.IsOnStrick = false;
                    onStricker.IsNonStrick = true;
                    context.playerInMatches.Update(onStricker);
                    await context.SaveChangesAsync();
                }

                nonStricker = await GetPlayerInMatchByMatchIdAndPlayerId(matchId, nonStrickerId);
                if (nonStricker != null)
                {
                    nonStricker.IsNonStrick = false;
                    nonStricker.IsOnStrick = true;
                    context.playerInMatches.Update(nonStricker);
                    await context.SaveChangesAsync();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> SetBowlerInBallingTeam(int matchId, int playerId)
        {
            try
            {
                int bowlingTeamType = await GetBowlingTeamTypeAsync(matchId);
                CricketTeam cricketTeam = await cricketTeamService.GetCricketTeamByMatchIdAndTeamTypeAsync(matchId, bowlingTeamType);
                PlayerInMatch currentBowler = await GetCurrentBowlerByMatchIdAndCricketTeamId(matchId, cricketTeam.Id);
                if(currentBowler != null)
                {
                    currentBowler.IsCurrentBowler = false;
                    await context.SaveChangesAsync();
                }
                PlayerInMatch playerInMatch = await GetPlayerInMatchByMatchIdAndPlayerId(matchId, playerId);
                if(playerInMatch != null)
                {
                    playerInMatch.IsCurrentBowler = true;
                    await context.SaveChangesAsync();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> GetBowlingTeamTypeAsync(int matchId)
        {
            TeamGroup teamGroup = (TeamGroup)await GetBattingTeamAsync(matchId);
            teamGroup = SwapEnum.swapTeamGroup(teamGroup);
            return (int)teamGroup;
        }

        public async Task<bool> SetBatsmanInBattingTeam(int matchId, int playerId)
        {
            try
            {
                int battingTeamType = await GetBattingTeamAsync(matchId);
                CricketTeam cricketTeam = await cricketTeamService.GetCricketTeamByMatchIdAndTeamTypeAsync(matchId, battingTeamType);
                var nonStricker = await GetNonStrickerByMatchIdAndCricketTeamId(matchId, cricketTeam.Id);
                var onStricker = await GetOnStrickerByMatchIdAndCricketTeamId(matchId, cricketTeam.Id);
                var currentPlayer = await GetPlayerInMatchByMatchIdAndPlayerId(matchId, playerId);
                if(nonStricker == null)
                {
                    currentPlayer.IsNonStrick = true;
                }
                else if(onStricker == null)
                {
                    currentPlayer.IsOnStrick = true;
                }
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> OutBatsManInBattingTeam(int matchId, int playerId)
        {
            try
            {
                int battingTeamType = await GetBattingTeamAsync(matchId);
                CricketTeam cricketTeam = await cricketTeamService.GetCricketTeamByMatchIdAndTeamTypeAsync(matchId, battingTeamType);
                var currentBatsman = await GetPlayerInMatchByMatchIdAndPlayerId(matchId, playerId);
                if (currentBatsman.IsOnStrick)
                {
                    currentBatsman.IsOnStrick = false;
                }
                else if (currentBatsman.IsNonStrick)
                {
                    currentBatsman.IsNonStrick = false;
                }
                currentBatsman.IsOut = true;
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
