﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Service.Player
{
    public interface IPlayerService
    {
        Task<Domain.Player> RegistrationPlayer(Domain.Player player);
    }
}
