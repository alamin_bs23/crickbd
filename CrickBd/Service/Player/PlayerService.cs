﻿using CrickBd.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Service.Player
{
    public class PlayerService : IPlayerService
    {
        private readonly CricketDbContext context;
        public PlayerService(CricketDbContext context)
        {
            this.context = context;
        }
        public async Task<Domain.Player> RegistrationPlayer(Domain.Player player)
        {
            if (player == null)
            {
                throw new NullReferenceException(nameof(player));
            }
            await context.Users.AddAsync(player);
            await context.SaveChangesAsync();
            return player;
        }
    }
}
