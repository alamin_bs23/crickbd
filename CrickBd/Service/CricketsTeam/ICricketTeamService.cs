﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Service.CricketsTeam 
{
    public interface ICricketTeamService
    {
        Task<Domain.CricketTeam> GetCricketTeamByMatchIdAndTeamTypeAsync(int matchId, int teamType);
    }
}
