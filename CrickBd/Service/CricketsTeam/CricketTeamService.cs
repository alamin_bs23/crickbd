﻿using CrickBd.Data;
using CrickBd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Service.CricketsTeam
{
    public class CricketTeamService : ICricketTeamService
    {
        private readonly CricketDbContext context;

        public CricketTeamService
            (
                CricketDbContext context
            )
        {
            this.context = context;
        }

        public Task<Domain.CricketTeam> GetCricketTeamByMatchIdAndTeamTypeAsync(int matchId, int teamType)
        {
            try
            {
                CricketTeam cricketTeam = context.CricketTeams.Where(x => x.matchId == matchId && x.TeamType == teamType).FirstOrDefault();
                return Task.FromResult(cricketTeam);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
