﻿using CrickBd.Data;
using CrickBd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Service.MatchOverEvent
{
    public class MatchOverService : IMatchOverService
    {

        private readonly CricketDbContext context;

        public MatchOverService(CricketDbContext context)
        {
            this.context = context;
        }

        public MatchOver GetMatchOver(int cricketTeamId, int playerId , bool isFinished = false)
        {
            try
            {
                MatchOver matchOver = context.MatchOvers.Where(x => x.CricketTeamId == cricketTeamId && x.PlayerId == playerId && x.IsFinished == isFinished).FirstOrDefault();
                return matchOver;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void InsertMatchOver(MatchOver matchOver)
        {
            try
            {
                if(matchOver == null)
                {
                    throw new NullReferenceException(nameof(matchOver));
                }
                context.MatchOvers.Add(matchOver);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UpdateMatchOver(MatchOver matchOver , int run)
        {
            try
            {
                matchOver.TotalRun += run;
                Ball ball = new Ball
                {
                    Run = run,
                    MatchOverId = matchOver.Id
                };
                context.Balls.Add(ball);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
