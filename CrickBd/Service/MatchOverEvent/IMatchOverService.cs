﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrickBd.Domain;

namespace CrickBd.Service.MatchOverEvent
{
    public interface IMatchOverService 
    {
        void InsertMatchOver(MatchOver matchOver);
        MatchOver GetMatchOver(int cricketTeamId, int playerId , bool isFinished = false);
        void UpdateMatchOver(MatchOver matchOver , int run);
    }
}
