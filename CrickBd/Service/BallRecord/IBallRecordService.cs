﻿using CrickBd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Service.BallRecord
{
    public interface IBallRecordService
    {
        void insertTeamBallsRecordAsync(TeamBallsRecords teamBallsRecords);
        Task<IList<TeamBallsRecords>> GetTeamBallsRecordsByCricketTeamId(int cricketTeamId);
    }
}
