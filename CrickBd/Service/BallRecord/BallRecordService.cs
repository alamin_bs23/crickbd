﻿using CrickBd.Data;
using CrickBd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Service.BallRecord
{
    public class BallRecordService : IBallRecordService
    {
        private readonly CricketDbContext context;

        public BallRecordService(CricketDbContext context)
        {
            this.context = context;
        }

        public Task<IList<TeamBallsRecords>> GetTeamBallsRecordsByCricketTeamId(int cricketTeamId)
        {
            try
            {
                IList<TeamBallsRecords> teamBallsRecords = context.TeamBallsRecords.Where(x => x.CricketTeamId == cricketTeamId).ToList();
                return Task.FromResult(teamBallsRecords);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void insertTeamBallsRecordAsync(TeamBallsRecords teamBallsRecords)
        {
            try
            {
                context.TeamBallsRecords.Add(teamBallsRecords);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
