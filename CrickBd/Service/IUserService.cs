﻿using CrickBd.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Service
{
    public interface IUserService
    {
        Task<AuthenticateResponse> AuthenticateAsync(AuthenticateRequest model);
        Task<IEnumerable<Domain.Player>> GetAllByUserNameAsync(string userName);
        Task<Domain.Player> GetByIdAsync(int id);
        Task<Domain.Player> GetByUserNameAsync(string userName);
        Task<Domain.Player> GetByUserNameAndPasswordAsync(string userName, string password);
    }
}
