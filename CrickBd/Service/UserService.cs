﻿using CrickBd.Data;
using CrickBd.Helper;
using CrickBd.Model;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CrickBd.Service
{
    public class UserService : IUserService
    {
        private readonly CricketDbContext context;
        private readonly AppSettings appSettings;

        public UserService(
            IOptions<AppSettings> appSettings,
            CricketDbContext context
            )
        {
            this.appSettings = appSettings.Value;
            this.context = context;
        }
        private Task<IEnumerable<Domain.Player>> GetAllUsersAsync()
        {
            var users = context.Users.AsEnumerable();
            return Task.FromResult(users);
        }
        public async Task<AuthenticateResponse> AuthenticateAsync(AuthenticateRequest model)
        {
            var user = await GetByUserNameAndPasswordAsync(model.Username, model.Password);
            if (user == null) return null;
            var token = generateJwtToken(user);
            return new AuthenticateResponse(user, token);
        }

        public Task<IEnumerable<Domain.Player>> GetAllByUserNameAsync(string userName)
        {
            var users = context.Users.Where(x => (userName == null || userName == "" || x.UserName.ToLower().Contains(userName.Trim().ToLower())));
            return Task.FromResult(users.AsEnumerable()); 
        }

        public async Task<Domain.Player> GetByIdAsync(int id)
        {
            var users = await GetAllUsersAsync();
            return users.Where(x => x.Id == id).SingleOrDefault();
        }

        // helper methods

        private string generateJwtToken(Domain.Player player)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", player.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public async Task<Domain.Player> GetByUserNameAsync(string userName)
        {
            var users = await GetAllUsersAsync();
            return users.Where(x => x.UserName == userName).SingleOrDefault();
        }

        public async Task<Domain.Player> GetByUserNameAndPasswordAsync(string userName, string password)
        {
            var users = await GetAllUsersAsync();
            return users.Where(x => x.UserName == userName && x.Password == password).SingleOrDefault();
        }
    }
}
