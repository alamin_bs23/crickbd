﻿using CrickBd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Service.Team
{
    public interface ITeamService
    {
        public Task<CricketTeam> CreateOrUpdateTeamAsync(CricketTeam team);
        public Task<CricketTeam> GetCricketTeamByMatchIdWithTeamTypeAsync(int matchId, int teamType);
        public Task InsertTeamMemberAsync(PlayerInMatch playerInMatch);
        public Task<bool> IsTeamCreatedByTeamTypeAndMatchIdAsync(int teamType, int matchId);
        public Task<IList<Domain.Player>> GetTeamMemberAsync(int matchId, int teamType);
        public Task UpdateNonStricker(int matchId , int plyaerId , bool isNonStricker);
        public Task UpdateOnStricker(int matchId, int playerId, bool isOnStricker);
        public Task UpdateOut(int matchId, int playerId, bool isOut);
        public Task UpdateAsCurrentBowler(int matchId, int playerId, bool isCurrentBowler);
        public Task<int> GetTeamScore(int matchId, int teamType);
        public Task<int> UpdateTeamScore(int matchId, int teamType, int run, int ballStatus);
    }
}
