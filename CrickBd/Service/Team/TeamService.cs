﻿using CrickBd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrickBd.Data;
using CrickBd.EventHandler.RunEvent;
using CrickBd.EventHandler;

namespace CrickBd.Service.Team
{
    public class TeamService : ITeamService
    {
        private readonly CricketDbContext context;
        private readonly IMatchService matchService;

        public TeamService(
            CricketDbContext context,
            IMatchService matchService
            )
        {
            this.context = context;
            this.matchService = matchService;
        }

        public async Task<CricketTeam> CreateOrUpdateTeamAsync(CricketTeam team)
        {
            try
            {
                var cricketTeam = await GetCricketTeamByMatchIdWithTeamTypeAsync(team.matchId, team.TeamType);
                if(cricketTeam == null)
                {
                    await context.CricketTeams.AddAsync(team);
                }
                else
                {
                    cricketTeam.TeamName = team.TeamName;
                    context.CricketTeams.Update(cricketTeam);
                }
                await context.SaveChangesAsync();
                return team;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Task<CricketTeam> GetCricketTeamByMatchIdWithTeamTypeAsync(int matchId, int teamType)
        {
            CricketTeam cricketTeam = context.CricketTeams.Where(x => x.matchId == matchId && x.TeamType == teamType).FirstOrDefault();
            return Task.FromResult(cricketTeam);
        }

        public Task<IList<Domain.Player>> GetTeamMemberAsync(int matchId, int teamType)
        {
            try
            {
                IList<Domain.Player> players = (from pl in context.Users
                                               join plim in context.playerInMatches on pl.Id equals plim.PlayerId
                                               join m in context.Matchs on plim.MatchId equals m.Id
                                               join ct in context.CricketTeams on plim.CricketTeamId equals ct.Id
                                               where (m.Id == matchId && ct.TeamType == teamType)
                                               select pl).ToList();
                return Task.FromResult(players);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Task<int> GetTeamScore(int matchId, int teamType)
        {
            try
            {
                CricketTeam cricketTeam = context.CricketTeams.Where(x => x.matchId == matchId && x.TeamType == teamType).FirstOrDefault();
                int totalRun = cricketTeam == null ? 0 : cricketTeam.TotalRun;
                return Task.FromResult(totalRun);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task InsertTeamMemberAsync(PlayerInMatch playerInMatch)
        {
            if(playerInMatch == null)
            {
                throw new NullReferenceException(nameof(playerInMatch));
            }
            try
            {
                context.playerInMatches.Add(playerInMatch);
                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> IsTeamCreatedByTeamTypeAndMatchIdAsync(int teamType, int matchId)
        {
            try
            {
                CricketTeam cricketTeam = await GetCricketTeamByMatchIdWithTeamTypeAsync(matchId: matchId, teamType: teamType);
                bool isExistCricketTeam = cricketTeam == null ? false : true;
                return isExistCricketTeam;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task UpdateAsCurrentBowler(int matchId, int playerId, bool isCurrentBowler)
        {
            try
            {
                PlayerInMatch playerInMatch = await matchService.GetPlayerInMatchByMatchIdAndPlayerId(matchId, playerId);
                if(playerInMatch != null)
                {
                    playerInMatch.IsCurrentBowler = isCurrentBowler;
                }
                await context.SaveChangesAsync();
            }   
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task UpdateNonStricker(int matchId , int plyaerId, bool isNonStricker)
        {
            try
            {
                PlayerInMatch playerInMatch = await matchService.GetPlayerInMatchByMatchIdAndPlayerId(matchId, plyaerId);
                if(playerInMatch != null)
                {
                    playerInMatch.IsNonStrick = isNonStricker; 
                }
                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task UpdateOnStricker(int matchId, int playerId, bool isOnStricker)
        {
            try
            {
                PlayerInMatch playerInMatch = await matchService.GetPlayerInMatchByMatchIdAndPlayerId(matchId, playerId);
                if(playerInMatch != null)
                {
                    playerInMatch.IsOnStrick = isOnStricker;
                }
                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task UpdateOut(int matchId, int playerId, bool isOut)
        {
            try
            {
                PlayerInMatch playerInMatch = await matchService.GetPlayerInMatchByMatchIdAndPlayerId(matchId, playerId);
                if(playerInMatch != null)
                {
                    playerInMatch.IsOut = isOut;
                }
                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> UpdateTeamScore(int matchId, int teamType, int run, int ballStatus)
        {
            try
            {
                CricketTeam cricketTeam = await GetCricketTeamByMatchIdWithTeamTypeAsync(matchId, teamType);
                cricketTeam.TotalRun += run;
                await context.SaveChangesAsync();
                await InvokeRunUpdateEvent.Invoke(new RunDomainEvent(cricketTeam,run,matchId,teamType,ballStatus));
                return cricketTeam.TotalRun;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
