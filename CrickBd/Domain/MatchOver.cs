﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Domain
{
    public class MatchOver
    {
        [Key]
        public int Id { get; set; } 
        public int TotalRun { get; set; } 
        public int PlayerId { get; set; } 
        public int MatchId { get; set; } 
        public int CricketTeamId { get; set; } 
        public bool IsFinished { get; set; } 
        public virtual CricketTeam CricketTeam { get; set; } 
        public virtual Match Match { get; set; } 
        public virtual Player Player { get; set; } 
        public virtual ICollection<Ball> Balls { get; set; } 
    }
}
