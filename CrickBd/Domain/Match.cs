﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Domain
{
    public class Match
    {
        public int Id { get; set; } 
        public string MatchName { get; set; } 
        public string MatchType { get; set; } 
        public int CreatedBy { get; set; } 
        public int NumberOfPlayer { get; set; } 
        public int TossWinnerGroup { get; set; } 
        public int WhichGroupBattingFirst { get; set; } 
        public DateTime CreatedOn { get; set; }  
        public virtual Player Player{ get; set; } 
        public virtual ICollection<PlayerInMatch> PlayerInMatches { get; set; } 
        public virtual ICollection<MatchOver> MatchOvers { get; set; }  
    }
}
