﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Domain
{
    public class Batting
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public string BattingName { get; set; } 
        public int TotalRun { get; set; }
        public int TotalMatch { get; set; }
        public decimal Avarage { get; set; }
        public Player User { get; set; } 
    }
}
