﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Domain
{
    public class TeamBallsRecords
    {
        public int Id { get; set; }
        public int CricketTeamId { get; set; }
        public int Run { get; set; }
        public int BallStatus { get; set; }
        public virtual CricketTeam CricketTeam { get; set; } 
    }
}
