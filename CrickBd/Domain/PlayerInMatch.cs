﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Domain
{
    public class PlayerInMatch
    {
        [Key]
        public int Id { get; set; } 
        public int MatchId { get; set; } 
        public int PlayerId { get; set; } 
        public int CricketTeamId { get; set; } 
        public int BattingCurrentRun { get;set; } 
        public string PlayerName { get; set; } 
        public int BattingBallCount { get; set; }
        public int BowlerBallCount { get; set; } 
        public int BowlerGivenRun { get; set; }
        public bool IsOnStrick { get; set; } 
        public bool IsNonStrick { get; set; } 
        public bool IsOut { get; set; } 
        public bool IsCurrentBowler { get; set; }
        public Match Match { get; set; } 
        public Player Player { get; set; } 
        public virtual CricketTeam CricketTeams { get; set; } 
    } 
} 
