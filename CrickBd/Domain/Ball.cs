﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Domain
{
    public class Ball
    {
        [Key]
        public int Id { get; set; } 
        public int MatchOverId { get; set; } 
        public int Run { get; set; } 
        public MatchOver MatchOver { get; set; } 
    }
}
