﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Domain
{
    public class CricketTeam
    {
        public int Id { get; set; } 
        public int matchId { get; set; } 
        public string TeamName { get; set; } 
        public int TeamType { get; set; } 
        public int TotalRun { get; set; } 
        public virtual ICollection<PlayerInMatch> PlayerInMatches{ get; set; } 
        public virtual ICollection<MatchOver> MatchOvers{ get; set; } 
        public virtual ICollection<TeamBallsRecords> TeamBallsRecords { get; set; } 
    }
}
