﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrickBd.Migrations
{
    public partial class TotalRunColumnAddedToCricketTeam : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TotalRun",
                table: "CricketTeams",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalRun",
                table: "CricketTeams");
        }
    }
}
