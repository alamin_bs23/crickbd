﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrickBd.Migrations
{
    public partial class ballsTableUpadated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Balls_MatchOvers_MatchOverId",
                table: "Balls");

            migrationBuilder.DropColumn(
                name: "OverId",
                table: "Balls");

            migrationBuilder.AlterColumn<int>(
                name: "MatchOverId",
                table: "Balls",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: false);

            migrationBuilder.AddForeignKey(
                name: "FK_Balls_MatchOvers_MatchOverId",
                table: "Balls",
                column: "MatchOverId",
                principalTable: "MatchOvers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Balls_MatchOvers_MatchOverId",
                table: "Balls");

            migrationBuilder.AlterColumn<int>(
                name: "MatchOverId",
                table: "Balls",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "OverId",
                table: "Balls",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_Balls_MatchOvers_MatchOverId",
                table: "Balls",
                column: "MatchOverId",
                principalTable: "MatchOvers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
