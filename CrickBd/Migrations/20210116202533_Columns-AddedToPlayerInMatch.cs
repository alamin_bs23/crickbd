﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrickBd.Migrations
{
    public partial class ColumnsAddedToPlayerInMatch : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsNonStrick",
                table: "playerInMatches",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsOnStrick",
                table: "playerInMatches",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsOut",
                table: "playerInMatches",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsNonStrick",
                table: "playerInMatches");

            migrationBuilder.DropColumn(
                name: "IsOnStrick",
                table: "playerInMatches");

            migrationBuilder.DropColumn(
                name: "IsOut",
                table: "playerInMatches");
        }
    }
}
