﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrickBd.Migrations
{
    public partial class MatchTypeAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PlayerGroup",
                table: "PlayerInMatch",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PlayerGroup",
                table: "PlayerInMatch");
        }
    }
}
