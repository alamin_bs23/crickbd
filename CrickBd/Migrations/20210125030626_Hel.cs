﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrickBd.Migrations
{
    public partial class Hel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TotalBall",
                table: "playerInMatches",
                newName: "BowlerGivenRun");

            migrationBuilder.RenameColumn(
                name: "PlayerRun",
                table: "playerInMatches",
                newName: "BowlerBallCount");

            migrationBuilder.AddColumn<int>(
                name: "BattingBallCount",
                table: "playerInMatches",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BattingCurrentRun",
                table: "playerInMatches",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BattingBallCount",
                table: "playerInMatches");

            migrationBuilder.DropColumn(
                name: "BattingCurrentRun",
                table: "playerInMatches");

            migrationBuilder.RenameColumn(
                name: "BowlerGivenRun",
                table: "playerInMatches",
                newName: "TotalBall");

            migrationBuilder.RenameColumn(
                name: "BowlerBallCount",
                table: "playerInMatches",
                newName: "PlayerRun");
        }
    }
}
