﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrickBd.Migrations
{
    public partial class AddCreatedByToMatch : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Matchs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PlayerId",
                table: "Matchs",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Matchs_PlayerId",
                table: "Matchs",
                column: "PlayerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Matchs_Users_PlayerId",
                table: "Matchs",
                column: "PlayerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Matchs_Users_PlayerId",
                table: "Matchs");

            migrationBuilder.DropIndex(
                name: "IX_Matchs_PlayerId",
                table: "Matchs");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Matchs");

            migrationBuilder.DropColumn(
                name: "PlayerId",
                table: "Matchs");
        }
    }
}
