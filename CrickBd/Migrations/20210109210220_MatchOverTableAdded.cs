﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrickBd.Migrations
{
    public partial class MatchOverTableAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ball_MatchOver_MatchOverId",
                table: "Ball");

            migrationBuilder.DropForeignKey(
                name: "FK_MatchOver_CricketTeams_CricketTeamId",
                table: "MatchOver");

            migrationBuilder.DropForeignKey(
                name: "FK_MatchOver_Matchs_MatchId",
                table: "MatchOver");

            migrationBuilder.DropForeignKey(
                name: "FK_MatchOver_Users_PlayerId",
                table: "MatchOver");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MatchOver",
                table: "MatchOver");

            migrationBuilder.RenameTable(
                name: "MatchOver",
                newName: "MatchOvers");

            migrationBuilder.RenameIndex(
                name: "IX_MatchOver_PlayerId",
                table: "MatchOvers",
                newName: "IX_MatchOvers_PlayerId");

            migrationBuilder.RenameIndex(
                name: "IX_MatchOver_MatchId",
                table: "MatchOvers",
                newName: "IX_MatchOvers_MatchId");

            migrationBuilder.RenameIndex(
                name: "IX_MatchOver_CricketTeamId",
                table: "MatchOvers",
                newName: "IX_MatchOvers_CricketTeamId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MatchOvers",
                table: "MatchOvers",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Ball_MatchOvers_MatchOverId",
                table: "Ball",
                column: "MatchOverId",
                principalTable: "MatchOvers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MatchOvers_CricketTeams_CricketTeamId",
                table: "MatchOvers",
                column: "CricketTeamId",
                principalTable: "CricketTeams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MatchOvers_Matchs_MatchId",
                table: "MatchOvers",
                column: "MatchId",
                principalTable: "Matchs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MatchOvers_Users_PlayerId",
                table: "MatchOvers",
                column: "PlayerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ball_MatchOvers_MatchOverId",
                table: "Ball");

            migrationBuilder.DropForeignKey(
                name: "FK_MatchOvers_CricketTeams_CricketTeamId",
                table: "MatchOvers");

            migrationBuilder.DropForeignKey(
                name: "FK_MatchOvers_Matchs_MatchId",
                table: "MatchOvers");

            migrationBuilder.DropForeignKey(
                name: "FK_MatchOvers_Users_PlayerId",
                table: "MatchOvers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MatchOvers",
                table: "MatchOvers");

            migrationBuilder.RenameTable(
                name: "MatchOvers",
                newName: "MatchOver");

            migrationBuilder.RenameIndex(
                name: "IX_MatchOvers_PlayerId",
                table: "MatchOver",
                newName: "IX_MatchOver_PlayerId");

            migrationBuilder.RenameIndex(
                name: "IX_MatchOvers_MatchId",
                table: "MatchOver",
                newName: "IX_MatchOver_MatchId");

            migrationBuilder.RenameIndex(
                name: "IX_MatchOvers_CricketTeamId",
                table: "MatchOver",
                newName: "IX_MatchOver_CricketTeamId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MatchOver",
                table: "MatchOver",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Ball_MatchOver_MatchOverId",
                table: "Ball",
                column: "MatchOverId",
                principalTable: "MatchOver",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MatchOver_CricketTeams_CricketTeamId",
                table: "MatchOver",
                column: "CricketTeamId",
                principalTable: "CricketTeams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MatchOver_Matchs_MatchId",
                table: "MatchOver",
                column: "MatchId",
                principalTable: "Matchs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MatchOver_Users_PlayerId",
                table: "MatchOver",
                column: "PlayerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
