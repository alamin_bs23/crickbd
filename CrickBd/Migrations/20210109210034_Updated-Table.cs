﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrickBd.Migrations
{
    public partial class UpdatedTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlayerInMatch_CricketTeams_CricketTeamsId",
                table: "PlayerInMatch");

            migrationBuilder.DropForeignKey(
                name: "FK_PlayerInMatch_MatchOver_MatchOverId",
                table: "PlayerInMatch");

            migrationBuilder.DropForeignKey(
                name: "FK_PlayerInMatch_Matchs_MatchId",
                table: "PlayerInMatch");

            migrationBuilder.DropForeignKey(
                name: "FK_PlayerInMatch_Users_PlayerId",
                table: "PlayerInMatch");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlayerInMatch",
                table: "PlayerInMatch");

            migrationBuilder.DropIndex(
                name: "IX_PlayerInMatch_CricketTeamsId",
                table: "PlayerInMatch");

            migrationBuilder.DropIndex(
                name: "IX_PlayerInMatch_MatchOverId",
                table: "PlayerInMatch");

            migrationBuilder.DropColumn(
                name: "CricketTeam",
                table: "PlayerInMatch");

            migrationBuilder.DropColumn(
                name: "CricketTeamsId",
                table: "PlayerInMatch");

            migrationBuilder.DropColumn(
                name: "MatchOverId",
                table: "PlayerInMatch");

            migrationBuilder.RenameTable(
                name: "PlayerInMatch",
                newName: "playerInMatches");

            migrationBuilder.RenameColumn(
                name: "OverId",
                table: "playerInMatches",
                newName: "CricketTeamId");

            migrationBuilder.RenameIndex(
                name: "IX_PlayerInMatch_PlayerId",
                table: "playerInMatches",
                newName: "IX_playerInMatches_PlayerId");

            migrationBuilder.RenameIndex(
                name: "IX_PlayerInMatch_MatchId",
                table: "playerInMatches",
                newName: "IX_playerInMatches_MatchId");

            migrationBuilder.AddColumn<int>(
                name: "CricketTeamId",
                table: "MatchOver",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MatchId",
                table: "MatchOver",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PlayerId",
                table: "MatchOver",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "teamId",
                table: "MatchOver",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_playerInMatches",
                table: "playerInMatches",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_MatchOver_CricketTeamId",
                table: "MatchOver",
                column: "CricketTeamId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchOver_MatchId",
                table: "MatchOver",
                column: "MatchId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchOver_PlayerId",
                table: "MatchOver",
                column: "PlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_playerInMatches_CricketTeamId",
                table: "playerInMatches",
                column: "CricketTeamId");

            migrationBuilder.AddForeignKey(
                name: "FK_MatchOver_CricketTeams_CricketTeamId",
                table: "MatchOver",
                column: "CricketTeamId",
                principalTable: "CricketTeams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MatchOver_Matchs_MatchId",
                table: "MatchOver",
                column: "MatchId",
                principalTable: "Matchs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MatchOver_Users_PlayerId",
                table: "MatchOver",
                column: "PlayerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_playerInMatches_CricketTeams_CricketTeamId",
                table: "playerInMatches",
                column: "CricketTeamId",
                principalTable: "CricketTeams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_playerInMatches_Matchs_MatchId",
                table: "playerInMatches",
                column: "MatchId",
                principalTable: "Matchs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_playerInMatches_Users_PlayerId",
                table: "playerInMatches",
                column: "PlayerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MatchOver_CricketTeams_CricketTeamId",
                table: "MatchOver");

            migrationBuilder.DropForeignKey(
                name: "FK_MatchOver_Matchs_MatchId",
                table: "MatchOver");

            migrationBuilder.DropForeignKey(
                name: "FK_MatchOver_Users_PlayerId",
                table: "MatchOver");

            migrationBuilder.DropForeignKey(
                name: "FK_playerInMatches_CricketTeams_CricketTeamId",
                table: "playerInMatches");

            migrationBuilder.DropForeignKey(
                name: "FK_playerInMatches_Matchs_MatchId",
                table: "playerInMatches");

            migrationBuilder.DropForeignKey(
                name: "FK_playerInMatches_Users_PlayerId",
                table: "playerInMatches");

            migrationBuilder.DropIndex(
                name: "IX_MatchOver_CricketTeamId",
                table: "MatchOver");

            migrationBuilder.DropIndex(
                name: "IX_MatchOver_MatchId",
                table: "MatchOver");

            migrationBuilder.DropIndex(
                name: "IX_MatchOver_PlayerId",
                table: "MatchOver");

            migrationBuilder.DropPrimaryKey(
                name: "PK_playerInMatches",
                table: "playerInMatches");

            migrationBuilder.DropIndex(
                name: "IX_playerInMatches_CricketTeamId",
                table: "playerInMatches");

            migrationBuilder.DropColumn(
                name: "CricketTeamId",
                table: "MatchOver");

            migrationBuilder.DropColumn(
                name: "MatchId",
                table: "MatchOver");

            migrationBuilder.DropColumn(
                name: "PlayerId",
                table: "MatchOver");

            migrationBuilder.DropColumn(
                name: "teamId",
                table: "MatchOver");

            migrationBuilder.RenameTable(
                name: "playerInMatches",
                newName: "PlayerInMatch");

            migrationBuilder.RenameColumn(
                name: "CricketTeamId",
                table: "PlayerInMatch",
                newName: "OverId");

            migrationBuilder.RenameIndex(
                name: "IX_playerInMatches_PlayerId",
                table: "PlayerInMatch",
                newName: "IX_PlayerInMatch_PlayerId");

            migrationBuilder.RenameIndex(
                name: "IX_playerInMatches_MatchId",
                table: "PlayerInMatch",
                newName: "IX_PlayerInMatch_MatchId");

            migrationBuilder.AddColumn<int>(
                name: "CricketTeam",
                table: "PlayerInMatch",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CricketTeamsId",
                table: "PlayerInMatch",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MatchOverId",
                table: "PlayerInMatch",
                type: "int",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlayerInMatch",
                table: "PlayerInMatch",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerInMatch_CricketTeamsId",
                table: "PlayerInMatch",
                column: "CricketTeamsId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerInMatch_MatchOverId",
                table: "PlayerInMatch",
                column: "MatchOverId");

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerInMatch_CricketTeams_CricketTeamsId",
                table: "PlayerInMatch",
                column: "CricketTeamsId",
                principalTable: "CricketTeams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerInMatch_MatchOver_MatchOverId",
                table: "PlayerInMatch",
                column: "MatchOverId",
                principalTable: "MatchOver",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerInMatch_Matchs_MatchId",
                table: "PlayerInMatch",
                column: "MatchId",
                principalTable: "Matchs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerInMatch_Users_PlayerId",
                table: "PlayerInMatch",
                column: "PlayerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
