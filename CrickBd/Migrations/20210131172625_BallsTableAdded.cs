﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrickBd.Migrations
{
    public partial class BallsTableAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ball_MatchOvers_MatchOverId",
                table: "Ball");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Ball",
                table: "Ball");

            migrationBuilder.RenameTable(
                name: "Ball",
                newName: "Balls");

            migrationBuilder.RenameIndex(
                name: "IX_Ball_MatchOverId",
                table: "Balls",
                newName: "IX_Balls_MatchOverId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Balls",
                table: "Balls",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Balls_MatchOvers_MatchOverId",
                table: "Balls",
                column: "MatchOverId",
                principalTable: "MatchOvers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Balls_MatchOvers_MatchOverId",
                table: "Balls");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Balls",
                table: "Balls");

            migrationBuilder.RenameTable(
                name: "Balls",
                newName: "Ball");

            migrationBuilder.RenameIndex(
                name: "IX_Balls_MatchOverId",
                table: "Ball",
                newName: "IX_Ball_MatchOverId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Ball",
                table: "Ball",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Ball_MatchOvers_MatchOverId",
                table: "Ball",
                column: "MatchOverId",
                principalTable: "MatchOvers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
