﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrickBd.Migrations
{
    public partial class addCricketTeam : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PlayerGroup",
                table: "PlayerInMatch",
                newName: "CricketTeam");

            migrationBuilder.AddColumn<int>(
                name: "CricketTeamsId",
                table: "PlayerInMatch",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CricketTeams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    matchId = table.Column<int>(type: "int", nullable: false),
                    TeamName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TeamType = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CricketTeams", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlayerInMatch_CricketTeamsId",
                table: "PlayerInMatch",
                column: "CricketTeamsId");

            migrationBuilder.AddForeignKey(
                name: "FK_PlayerInMatch_CricketTeams_CricketTeamsId",
                table: "PlayerInMatch",
                column: "CricketTeamsId",
                principalTable: "CricketTeams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlayerInMatch_CricketTeams_CricketTeamsId",
                table: "PlayerInMatch");

            migrationBuilder.DropTable(
                name: "CricketTeams");

            migrationBuilder.DropIndex(
                name: "IX_PlayerInMatch_CricketTeamsId",
                table: "PlayerInMatch");

            migrationBuilder.DropColumn(
                name: "CricketTeamsId",
                table: "PlayerInMatch");

            migrationBuilder.RenameColumn(
                name: "CricketTeam",
                table: "PlayerInMatch",
                newName: "PlayerGroup");
        }
    }
}
