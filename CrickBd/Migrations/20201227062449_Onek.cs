﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CrickBd.Migrations
{
    public partial class Onek : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedOn",
                table: "Matchs",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "NumberOfPlayer",
                table: "Matchs",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "MatchOver",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TotalRun = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchOver", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ball",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OverId = table.Column<int>(type: "int", nullable: false),
                    Run = table.Column<int>(type: "int", nullable: false),
                    MatchOverId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ball", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ball_MatchOver_MatchOverId",
                        column: x => x.MatchOverId,
                        principalTable: "MatchOver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PlayerInMatch",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MatchId = table.Column<int>(type: "int", nullable: false),
                    PlayerId = table.Column<int>(type: "int", nullable: false),
                    OverId = table.Column<int>(type: "int", nullable: false),
                    PlayerRun = table.Column<int>(type: "int", nullable: false),
                    PlyaerName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TotalBall = table.Column<int>(type: "int", nullable: false),
                    MatchOverId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerInMatch", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlayerInMatch_MatchOver_MatchOverId",
                        column: x => x.MatchOverId,
                        principalTable: "MatchOver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PlayerInMatch_Matchs_MatchId",
                        column: x => x.MatchId,
                        principalTable: "Matchs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlayerInMatch_Users_PlayerId",
                        column: x => x.PlayerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Ball_MatchOverId",
                table: "Ball",
                column: "MatchOverId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerInMatch_MatchId",
                table: "PlayerInMatch",
                column: "MatchId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerInMatch_MatchOverId",
                table: "PlayerInMatch",
                column: "MatchOverId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerInMatch_PlayerId",
                table: "PlayerInMatch",
                column: "PlayerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Ball");

            migrationBuilder.DropTable(
                name: "PlayerInMatch");

            migrationBuilder.DropTable(
                name: "MatchOver");

            migrationBuilder.DropColumn(
                name: "CreatedOn",
                table: "Matchs");

            migrationBuilder.DropColumn(
                name: "NumberOfPlayer",
                table: "Matchs");
        }
    }
}
