﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrickBd.Migrations
{
    public partial class TossWinnerColomnAddedToMatch : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TossWinnerGroup",
                table: "Matchs",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "WhichGroupBattingFirst",
                table: "Matchs",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TossWinnerGroup",
                table: "Matchs");

            migrationBuilder.DropColumn(
                name: "WhichGroupBattingFirst",
                table: "Matchs");
        }
    }
}
