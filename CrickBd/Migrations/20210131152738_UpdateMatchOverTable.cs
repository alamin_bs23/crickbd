﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CrickBd.Migrations
{
    public partial class UpdateMatchOverTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MatchOvers_CricketTeams_CricketTeamId",
                table: "MatchOvers");

            migrationBuilder.DropColumn(
                name: "teamId",
                table: "MatchOvers");

            migrationBuilder.AlterColumn<int>(
                name: "CricketTeamId",
                table: "MatchOvers",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsFinished",
                table: "MatchOvers",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_MatchOvers_CricketTeams_CricketTeamId",
                table: "MatchOvers",
                column: "CricketTeamId",
                principalTable: "CricketTeams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MatchOvers_CricketTeams_CricketTeamId",
                table: "MatchOvers");

            migrationBuilder.DropColumn(
                name: "IsFinished",
                table: "MatchOvers");

            migrationBuilder.AlterColumn<int>(
                name: "CricketTeamId",
                table: "MatchOvers",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "teamId",
                table: "MatchOvers",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_MatchOvers_CricketTeams_CricketTeamId",
                table: "MatchOvers",
                column: "CricketTeamId",
                principalTable: "CricketTeams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
