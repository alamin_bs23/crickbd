﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Model
{
    public class Batting
    {
        public int Id { get; set; }
        public string BattingName { get; set; } 
        public int TotalRun { get; set; }
        public int TotalMatch { get; set; }
        public decimal Avarage { get; set; } 
    }
}
