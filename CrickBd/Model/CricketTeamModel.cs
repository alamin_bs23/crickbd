﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Model
{
    public class CricketTeamModel
    {
        public int Id { get; set; }
        public int matchId { get; set; }
        public string TeamName { get; set; }
        public int TeamType { get; set; }
    }
}
