﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Model.MatchModel
{
    public class TossModel
    {
        public int matchId { get; set; } 
        public int winnerTeam { get; set; }
        public int battingFirstTeam { get; set; }
        public int userId { get; set; }  
    }
}
