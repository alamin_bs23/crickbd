﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Model.MatchModel
{
    public class PlayerIdsModel
    {
        public int userId { get; set; } 
        public int teamType { get; set; }
        public int matchId { get; set; } 
        public int PlyaerId1 { get; set; }
        public int PlyaerId2 { get; set; }
        public int PlyaerId3 { get; set; } 
    }
}
