﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Model
{
    public class Match 
    {
        public int Id { get; set; }
        public string MatchName { get; set; }
        public string MatchType { get; set; }
        public int CreatedBy { get; set; }
        public int NumberOfPlayer { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
