﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Model
{
    public class AuthenticateResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Token { get; set; } 
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public AuthenticateResponse(Domain.Player player, string token)
        {
            Id = player.Id; 
            FirstName = player.FirstName;
            LastName = player.LastName;
            UserName = player.UserName;
            Email = player.Email;
            PhoneNumber = player.PhoneNumber;
            Token = token;
        }
    }
}
