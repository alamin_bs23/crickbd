﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Model
{
    public class TeamCreationModel
    {
        public int matchId { get; set; }
        public int TeamType { get; set; }
        public string TeamName { get; set; } 
        public string member1 { get; set; }
        public string member2 { get; set; }
        public string member3 { get; set; }
        public string member4 { get; set; }
        public string member5 { get; set; }
        public string member6 { get; set; }
        public string member7 { get; set; }
        public string member8 { get; set; }
        public string member9 { get; set; }
        public string member10 { get; set; }
        public string member11 { get; set; }   
    }
}
