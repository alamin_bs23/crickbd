﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Model
{
    public class RouteModel
    {
        public string Name { get; set; }
        public string Template { get; set; } 
    }
}
