﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrickBd.Model;
using CrickBd.Model.MatchModel;

namespace CrickBd.CricketMatch
{
    public interface ICricketMatch
    {
        public Task<Domain.Match> GetMatchByIdAsync();
        public Task DeletMatch();
        public IList<Domain.Match> GetMatchesByUserId();
        public IList<Domain.Match> GetAllMatch();
        public Task<int> GetNumberOfPlayerInMatchByMatchIdAndUserId();
        public Task UpdateTossWinner(TossModel model);
        public Task<bool> UpdateNonStricker(PlayerIdsModel model);
        public Task<bool> UpdateOnStricker(PlayerIdsModel model);
        public Task<bool> UpdatePlayerIsOut(PlayerIdsModel model);
        public Task<bool> UpdateAsCurrentBowler(PlayerIdsModel model);
    }
}
