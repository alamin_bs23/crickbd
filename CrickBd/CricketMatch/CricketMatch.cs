﻿using CrickBd.CricketMatch;
using CrickBd.Domain;
using CrickBd.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using CrickBd.Model.MatchModel;
using CrickBd.Service.Team;
using System.Reflection;

namespace CrickBd.CricketMatch
{
    public class CricketMatch : ICricketMatch
    {
        private readonly int matchId;
        private readonly int userId;

        public CricketMatch(
            int matchId,
            int userId
            )
        {
            this.matchId = matchId;
            this.userId = userId;
        }

        public async Task DeletMatch()
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                    await matchService.DeletMatchAsync(matchId);
                } 
               
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public IList<Match> GetAllMatch()
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                    List<Domain.Match> matches = matchService.GetAllMatches().ToList();
                    return matches;
                }
            }
            catch (Exception ex)
            {
                
                throw new Exception(ex.Message);
            }
        }

        public async Task<Domain.Match> GetMatchByIdAsync()
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                    Domain.Match match = await matchService.GetMatchByIdAsync(matchId);
                    return  match;
                } 
               
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public IList<Match> GetMatchesByUserId()
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                    List<Domain.Match> matches = matchService.GetMatchByUserId(userId).ToList();
                    return matches;
                }
                
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<int> GetNumberOfPlayerInMatchByMatchIdAndUserId()
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                    int plyaerCount = await matchService.GetNumberOfPlayerInMatch(this.matchId, this.userId);
                    return plyaerCount;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> UpdateAsCurrentBowler(PlayerIdsModel model)
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    ITeamService teamService = serviceScope.ServiceProvider.GetService<ITeamService>();
                    Type myType = typeof(PlayerIdsModel);
                    IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
                    foreach (var prop in props)
                    {
                        if (prop.ToString().Contains("PlyaerId"))
                        {
                            var value = prop.GetValue(model);
                            if ((int)value > 0)
                            {
                                await teamService.UpdateAsCurrentBowler(model.matchId, (int)value, true);
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> UpdateNonStricker(PlayerIdsModel model)
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    ITeamService teamService = serviceScope.ServiceProvider.GetService<ITeamService>();
                    Type myType = typeof(PlayerIdsModel);
                    IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
                    foreach(var prop in props)
                    {
                        if (prop.ToString().Contains("PlyaerId"))
                        {
                            var value = prop.GetValue(model);
                            if((int)value > 0)
                            {
                                await teamService.UpdateNonStricker(model.matchId, (int)value, true);
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> UpdateOnStricker(PlayerIdsModel model)
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    ITeamService teamService = serviceScope.ServiceProvider.GetService<ITeamService>();
                    Type myType = typeof(PlayerIdsModel);
                    IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
                    foreach (var prop in props)
                    {
                        if (prop.ToString().Contains("PlyaerId"))
                        {
                            var value = prop.GetValue(model);
                            if ((int)value > 0)
                            {
                                await teamService.UpdateOnStricker(model.matchId, (int)value, true);
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> UpdatePlayerIsOut(PlayerIdsModel model)
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    ITeamService teamService = serviceScope.ServiceProvider.GetService<ITeamService>();
                    Type myType = typeof(PlayerIdsModel);
                    IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
                    foreach (var prop in props)
                    {
                        if (prop.ToString().Contains("PlyaerId"))
                        {
                            var value = prop.GetValue(model);
                            if ((int)value > 0)
                            {
                                await teamService.UpdateOut(model.matchId, (int)value, true);
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task UpdateTossWinner(TossModel model)
        {
            using (var serviceScope = ServiceActivator.GetScope())
            {
                IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                await matchService.UpdateTossWinner(model);
            }
        }
    }
}
