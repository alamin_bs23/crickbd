﻿using CrickBd.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.CricketMatch 
{
    public class MatchFactory
    {
        public static CricketMatch GetMatch(int matchId , int userId)
        {
            return new CricketMatch(matchId , userId);
        }
    }
}
