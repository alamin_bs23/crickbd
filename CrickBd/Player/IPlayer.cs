﻿using CrickBd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Player
{
    public interface IPlayer
    {
        public Task<Batting> getCurrentBatting();
        public Task<int> getCurrentRun();
        public void AddRun(int run);
        public Task<bool> isActive();
        public Task<int> BallsInOver();
        public Task<int> TotalRunInOver(); 
    }
}
