﻿using CrickBd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Player
{
    public abstract class PlayerBridge
    {
        protected IPlayer player;
        public PlayerBridge(IPlayer player)
        {
            this.player = player;
        }
        public abstract Task<Batting> getCurrentBatting();
        public abstract Task<int> getCurrentRun();
        public abstract void AddRun(int run);
        public abstract Task<bool> isActive();
        public abstract Task<int> BallsInOver();
        public abstract Task<int> TotalRunInOver();
    }
}
