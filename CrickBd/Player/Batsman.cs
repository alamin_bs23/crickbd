﻿using CrickBd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Player
{
    public class Batsman : IPlayer
    {
        public int matchId , plyerId;
        public Batsman(int matchId , int plyerId)
        {
            this.matchId = matchId;
            this.plyerId = plyerId;
        }
        public void AddRun(int run)
        {
            Console.WriteLine("Run Added {0} to this Match {1}", run , this.matchId);
        }

        public Task<int> BallsInOver()
        {
            Console.WriteLine("BallsInOver in match {0}", this.matchId);
            return Task.FromResult(1);
        }

        public Task<Batting> getCurrentBatting()
        {
            Console.WriteLine("Current Batting is {1}", this.matchId);
            return Task.FromResult(new Batting());
        }

        public Task<int> getCurrentRun()
        {
            Console.WriteLine("Current Run {0}", this.matchId);
            return Task.FromResult(1);
        }

        public Task<bool> isActive()
        {
            Console.WriteLine("Current is Active {1}", this.matchId);
            return Task.FromResult(true);
        }

        public Task<int> TotalRunInOver()
        {
            Console.WriteLine("Total run {0}", this.matchId);
            return Task.FromResult(1);
        }
    }
}
