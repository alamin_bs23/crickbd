﻿using CrickBd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Player
{
    public class Baller : IPlayer
    {
        public int matchId, plyerId;
        public Baller(int matchId, int plyerId)
        {
            this.matchId = matchId;
            this.plyerId = plyerId;
        }
        public void AddRun(int run)
        {
            Console.WriteLine("Run Added to Baller {0} to this Match {1}", run, this.matchId);
        }

        public Task<int> BallsInOver()
        {
            Console.WriteLine("BallsInOver from baller in match {0}", this.matchId);
            return Task.FromResult(1);
        }

        public Task<Batting> getCurrentBatting()
        {
            Console.WriteLine("Current from boller Batting is {1}", this.matchId);
            return Task.FromResult(new Batting());
        }

        public Task<int> getCurrentRun()
        {
            return Task.FromResult(1);
        }

        public Task<bool> isActive()
        {
            return Task.FromResult(true);
        }

        public Task<int> TotalRunInOver()
        {
            return Task.FromResult(1);
        }
    }
}
