﻿using CrickBd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Player
{
    public class PlayerFactory : PlayerBridge
    {

        public PlayerFactory(IPlayer player) : base(player)
        {

        }
        
        public override void AddRun(int run)
        {
            player.AddRun(run);
        }

        public override Task<int> BallsInOver()
        {
            return player.BallsInOver();
        }

        public override Task<Batting> getCurrentBatting()
        {
            return player.getCurrentBatting();
        }

        public override Task<int> getCurrentRun()
        {
            return player.getCurrentRun();
        }

        public override Task<bool> isActive()
        {
            return player.isActive();
        }

        public override Task<int> TotalRunInOver()
        {
            return player.TotalRunInOver();
        }
    }
}
