"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlayerIdsModel = exports.BallStatus = exports.GroupType = exports.Player = exports.User = void 0;
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());
exports.User = User;
var Player = /** @class */ (function () {
    function Player() {
    }
    return Player;
}());
exports.Player = Player;
var GroupType;
(function (GroupType) {
    GroupType[GroupType["GroupA"] = 0] = "GroupA";
    GroupType[GroupType["GroupB"] = 1] = "GroupB";
})(GroupType = exports.GroupType || (exports.GroupType = {}));
var BallStatus;
(function (BallStatus) {
    BallStatus[BallStatus["NoBall"] = 1] = "NoBall";
    BallStatus[BallStatus["RightBall"] = 2] = "RightBall";
    BallStatus[BallStatus["WideBall"] = 3] = "WideBall";
    BallStatus[BallStatus["DadeBall"] = 4] = "DadeBall";
    BallStatus[BallStatus["LegBy"] = 5] = "LegBy";
})(BallStatus = exports.BallStatus || (exports.BallStatus = {}));
var PlayerIdsModel = /** @class */ (function () {
    function PlayerIdsModel() {
    }
    return PlayerIdsModel;
}());
exports.PlayerIdsModel = PlayerIdsModel;
//# sourceMappingURL=User.js.map