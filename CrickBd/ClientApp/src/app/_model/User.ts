export class User {
  id: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  token?: string;
}

export class Player{
  Id: number;
  FirstName: string;
  LastName: string;
  UserName: string;
  Email: string;
  Password: string;
  PhoneNumber: string;
}

export enum GroupType {
  GroupA = 0,
  GroupB = 1
}

export enum BallStatus {
  NoBall = 1,
  RightBall = 2,
  WideBall = 3,
  DadeBall = 4,
  LegBy = 5
}

export class PlayerIdsModel {
  userId: number;
  teamType: number;
  matchId: number;
  PlyaerId1: number;
  PlyaerId2: number;
  PlyaerId3: number;
}
