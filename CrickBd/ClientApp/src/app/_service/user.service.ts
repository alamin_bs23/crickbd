import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { User, Player } from '../_model/User';
import { HttpRequestService } from './httpRequest.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor
    (
      private http: HttpClient,
      private httpRequestService : HttpRequestService
    ) { }

  getAll() {
    return this.http.get<User[]>(`${environment.apiUrl}/users`)
  }
  getCurrentUser = function() {
    return new Promise(function (resolve, reject) {
      let currentUser = localStorage.getItem("currentUser");
      if (currentUser) {
        resolve(currentUser);
      }
      else {
        reject("User not found");
      }
    });
  }
  getCurrentUserId = function () {
    return new Promise(function (resolve, reject) {
      var currentUserId = localStorage.getItem("userId");
      if (currentUserId !== undefined) {
        resolve(currentUserId);
      }
      else {
        reject('User id not found');
      }
    });
  }
  currentUserId = function () {
    var currentUserId = localStorage.getItem("userId");
    if (currentUserId != undefined)
      return currentUserId;
    else return -1;
  }
  isUserExistByUserName(userName) {
    return this.httpRequestService.get(`api/users/exist?userName=${userName}`);
  }
}
