import { Injectable } from '@angular/core';
import { HttpRequestService } from './httpRequest.service';
import { User } from '../_model/User';

@Injectable({
  providedIn: 'root'
})
export class MatchService {
  matches;
  constructor(private httpRequestService: HttpRequestService) { }

  getAllMatches(){
    return this.httpRequestService.get('api/cricket/match/get/matches');
  }
  getAllMatchesByUserId(userId) {
    return this.httpRequestService.get('api/cricket/match/get/user/mathes?='+`${userId}`)
  }
  getMatchPlayerCount(matchId , userId) {
    return this.httpRequestService.get('api/cricket/match/player/count?matchId=' + `${parseInt(matchId)}` + '&userId=' + `${parseInt(userId)}`);
  }
  updateTossWinnerInMatch(matchId, winnerTeam, battingFirstTeam,userId) {
    return this.httpRequestService.post('api/cricket/match/toss/winner', { matchId, winnerTeam, battingFirstTeam, userId });
  }
  getBattingTeam(matchId) {
    return this.httpRequestService.get(`api/cricket/match/get/batting/team?matchId=${matchId}`);
  }
  updateNonStrickerInMatch(model) {
    return this.httpRequestService.post('api/cricket/match/add/batsman/nonstricke', model);
  }
  updateOnStrickerInMatch(model) {
    return this.httpRequestService.post('api/cricket/match/add/batsman/onstricke', model);
  }
  updateCurrentBowler(model) {
    return this.httpRequestService.post('api/cricket/match/update/current/bowler', model);
  }
  getCurrentBowler(matchId, teamType) {
    return fetch(`api/team/get/current/bowler?teamType=${teamType}&matchId=${matchId}`);
  }
  getCurrentNonStricker(matchId, teamType) {
    return fetch(`api/team/get/non/stricker?teamType=${teamType}&matchId=${matchId}`);
  }
  getCurrentOnStricker(matchId, teamType) {
    return fetch(`api/team/get/on/stricker?teamType=${teamType}&matchId=${matchId}`);
  }
  getBatsmanIsSelected(matchId) {
    return this.httpRequestService.get(`api/team/batsman/isselected?matchId=${matchId}`);
  }
  getBowlerIsSelected(matchId) {
    return this.httpRequestService.get(`api/team/bowler/isselected?matchId=${matchId}`);
  }
  getRecentBalls(matchId) {
    return fetch(`api/team/get/recent/ball?matchId=${matchId}`);
  }
  swapStrickerAndNonStricker(matchId) {
    return this.httpRequestService.get(`api/team/swap/stricker/nonstricker?matchId=${matchId}`)
  }
  setBowler(matchId, playerId) {
    return this.httpRequestService.get(`api/team/set/bowler?matchId=${matchId}&playerId=${playerId}`);
  }
  setBatsman(matchId, playerId) {
    return this.httpRequestService.get(`api/team/set/batsman?matchId=${matchId}&playerId=${playerId}`);
  }
  outBatsman(matchId, playerId) {
    return this.httpRequestService.get(`api/team/out/batsman?matchId=${matchId}&playerId=${playerId}`);
  }
}
