import { Injectable, Inject } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {
  private baseUrl;
  headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });
  options = {
    headers: this.headers
  }
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
  }
  
  post(url, model) {
    return this.http.post(this.baseUrl + url, model, this.options).toPromise();
  }
  get(url) {
    return this.http.get(this.baseUrl + url, this.options).toPromise();
  }
  getSubscrive(url) {
    return this.http.get(this.baseUrl + url, this.options);
  }
}
