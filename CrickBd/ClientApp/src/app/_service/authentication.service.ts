import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { User } from '../_model/User';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    return this.http.post<any>(`${environment.apiUrl}/users/authenticate`, { username, password })
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user.result));
        localStorage.setItem('userId', JSON.stringify(user.result.id));
        console.log(localStorage.getItem('userId'));
        console.log(user);
        console.log('userId : ', user.id);
        this.currentUserSubject.next(user);
        return user;
      }));
  }

  signup(model) {
    return this.http.post<any>(`${environment.apiUrl}/users/signup`, model)
      .pipe(map(user => {
        return user;
      }))
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('userId');
    this.currentUserSubject.next(null);
  }
  isLoggedIn = function() {
    return new Promise(function (resolve) {
        let currentUser = localStorage.getItem("currentUser");
        let isLogged = currentUser !== null ? true : false;
        resolve(isLogged);
    });
  }
}
