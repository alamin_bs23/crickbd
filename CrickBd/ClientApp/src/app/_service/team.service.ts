import { Injectable } from '@angular/core';
import { HttpRequestService } from './httpRequest.service';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private httpRequestService : HttpRequestService) { }

  checkIsCreatedTeam(matchId) {
    return this.httpRequestService.get('api/team/matches/count?matchId=' + `${matchId}`);
  }
  createTeam(model) {
    return this.httpRequestService.post('api/team/create', model);
  }
  getTeamMembers(teamType, matchId) {
    return this.httpRequestService.get(`api/team/get/members?teamType=${teamType}&matchId=${matchId}`);
  }
  getTeamScore(teamType, matchId) {
    return this.httpRequestService.get(`api/match/team/get/score?teamType=${teamType}&matchId=${matchId}`);
  }
  updateTeamScore(teamType, matchId, run, ballStatus) {
    return this.httpRequestService.get(`api/match/team/update/score?teamType=${teamType}&matchId=${matchId}&run=${run}&ballStatus=${ballStatus}`)
  }
}
