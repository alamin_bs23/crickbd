import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { GetCrickInfoService } from '../get-crick-info.service';
import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_service/authentication.service';
import { UserService } from '../_service/user.service';

@Component({
  selector: 'app-match-create',
  templateUrl: './match-create.component.html',
  styleUrls: ['./match-create.component.css']
})
export class MatchCreateComponent implements OnInit {
  match;
  loading = false;
  submitted = false;
  error: any;
  constructor(
    private formBuilder: FormBuilder,
    private getCrickInfoService: GetCrickInfoService,
    private authenticationService: AuthenticationService,
    private userService : UserService,
    private router : Router
  )
  {
    this.match = this.formBuilder.group({
      matchName: ['',Validators.required],
      matchType: ['',Validators.required],
      numberOfPlayer: [0,Validators.required],
      createdOn: [new Date(), Validators.required],
      CreatedBy:0
    });
  }
  get f() { return this.match.controls; }

  ngOnInit(): void {
  }
  onSubmit(match) {

    this.submitted = true;
    // stop here if form is invalid
    if (this.match.invalid) {
      return;
    }

    this.loading = true;

    this.authenticationService.isLoggedIn()
      .then(isLoggedIn => {
        if (isLoggedIn) {
          return this.userService.getCurrentUserId();
        }
        else {
          this.router.navigate(["/login"]);
        }
      }).then(userId => {
        if (userId !== "User id not found") {
          match.CreatedBy = Number(userId); 
          return this.getCrickInfoService.createMatch(match);
        }
      }).then(match => {
        this.loading = false;
        console.log(match);
      }).catch(error => console.log(error));
  }
}
