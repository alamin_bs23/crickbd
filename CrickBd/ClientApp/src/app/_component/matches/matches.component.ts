import { Component, OnInit } from '@angular/core';
import { MatchService } from '../../_service/match.service';
import { ShareDataService } from '../../_service/share-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent implements OnInit {
  matchesList;
  constructor(
    private matchService: MatchService,
    private shareDataService: ShareDataService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.matchService.getAllMatches().then(x => {
      this.matchesList = x;
      console.log(x);
    });
  }
  click(event) {
    console.log(event.target.id);
    this.shareDataService.changeMessage(event.target.id.toString());
    this.router.navigate(['/teamA'],
      { queryParams: { matchId: event.target.id} });
  }
}
