import { Component, OnInit } from '@angular/core';
import { GroupType, PlayerIdsModel } from '../../_model/User';
import { Route, ActivatedRoute, Router } from '@angular/router';
import { MatchService } from '../../_service/match.service';
import { UserService } from '../../_service/user.service';
import { TeamService } from '../../_service/team.service';
import * as $ from 'jquery';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { count } from 'rxjs/operators';

@Component({
  selector: 'app-toss',
  templateUrl: './toss.component.html',
  styleUrls: ['./toss.component.css']
})
export class TossComponent implements OnInit {
  IsOpenTossDiv = true;
  tossDescription = 'Now this is toss time.';
  tossResult;
  tossIsCompleted = false;
  makeDecition;
  tossDecisionIsPublished = false;
  winnerGroup;
  winnerDecision;
  doToss = 'Click the toss button';
  whichTeamWinnerTheToss;
  whichTeamChooseToBattingFirst;
  userId;
  matchId;
  teamMembers;
  giveInstraction;
  activeBattingButton = true;
  activeBallerButton = false;

  constructor(
    private route: ActivatedRoute,
    private matchService: MatchService,
    private userService: UserService,
    private teamService: TeamService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route
      .queryParams
      .subscribe(params => {
        this.matchId = params['matchId'];
      });
    this.userId = this.userService.currentUserId();
    this.checkingIsStateCompleted();
  }

  checkingIsStateCompleted() {
    this.matchService.getBatsmanIsSelected(this.matchId)
      .then(isSelectedBatsMans => {
        if (isSelectedBatsMans) {
          return this.matchService.getBowlerIsSelected(this.matchId);
        }
        else {
          return Promise.reject();
        }
      })
      .then(isSelectedBowler => {
        if (isSelectedBowler) {
          this.router.navigate(['/scoreboard'],
            { queryParams: { matchId: this.matchId } });
        }
      })
      .catch(ex => {
        console.log(ex);
      })
  }

  private randomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  createTos() {
    var rand = this.randomInteger(0,1);
    this.tossIsCompleted = true;
    this.tossDecisionIsPublished = false;
    this.makeDecition = 'Are you willing to batting first !';
    if (rand == 0) {
      this.tossResult = 'Group A';
      this.winnerGroup = 'Group A';
      this.whichTeamWinnerTheToss = GroupType.GroupA;
      this.whichTeamChooseToBattingFirst = GroupType.GroupA;
    }
    else {
      this.tossResult = 'Group B';
      this.winnerGroup = 'Group B';
      this.whichTeamWinnerTheToss = GroupType.GroupB;
      this.whichTeamChooseToBattingFirst = GroupType.GroupB;
    }
  }

  makeDecision(decision) {
    this.tossIsCompleted = false;
    this.tossDecisionIsPublished = true;

    if (decision === 'Yes') {
      this.winnerDecision = 'Batting';
    }
    else {
      this.winnerDecision = 'Balling';
      this.whichTeamChooseToBattingFirst = 1 - this.whichTeamChooseToBattingFirst;
    }
  }

  GoNext() {
    this.matchService.updateTossWinnerInMatch(Number(this.matchId), this.whichTeamWinnerTheToss, this.whichTeamChooseToBattingFirst, Number(this.userId))
      .then(success => {
        if (success) {
          return this.matchService.getBattingTeam(this.matchId);
        }
        else {
          return Promise.reject();
        }
      })
      .then(teamType => {
        return this.teamService.getTeamMembers(Number(teamType), Number(this.matchId));
      })
      .then(teamMembers => {
        console.log(teamMembers);
        this.IsOpenTossDiv = false;
        this.giveInstraction = 'Select The First Two Batsman!'
        this.teamMembers = teamMembers;
      })
      .catch(ex => {
        console.log(ex);
      });
  }

  selectFirstTwoBatsman() {
    let countSelectedBatsman = 0;
    let selectedBatsmanId = [];
    this.teamMembers.forEach(x => {
      var isChecked = $('#' + x.userName + '-input');
      if (isChecked[0].ariaChecked) {
        countSelectedBatsman++;
        selectedBatsmanId.push(x.id);
      }
    });

    let model = new PlayerIdsModel();
    model.userId = Number(this.userId);
    model.matchId = Number(this.matchId);

    this.matchService.getBattingTeam(this.matchId)
      .then(teamType => {
        model.teamType = Number(teamType);
        model.PlyaerId1 = Number(selectedBatsmanId[0]);
        model.PlyaerId2 = -1;
        model.PlyaerId3 = -1;
        return this.matchService.updateOnStrickerInMatch(model);
      })
      .then(isUpdatedOnStricker => {
        if (isUpdatedOnStricker) {
          model.PlyaerId1 = Number(selectedBatsmanId[1]);
          return this.matchService.updateNonStrickerInMatch(model);
        }
        else {
          return Promise.reject();
        }
      })
      .then(result => {
        console.log(result);
      })

    if (countSelectedBatsman >= 1 && selectedBatsmanId.length >= 1) {
      this.matchService.getBattingTeam(this.matchId)
        .then(teamType => {
          teamType = teamType === GroupType.GroupA ? GroupType.GroupB : GroupType.GroupA;
          return this.teamService.getTeamMembers(Number(teamType), Number(this.matchId));
        })
        .then(teamMembers => {
          this.teamMembers = teamMembers;
          this.giveInstraction = 'Select The First Baller!';
          this.activeBattingButton = false;
          this.activeBallerButton = true;
        });
    }
  }

  selectFirstBowler() {
    let countSelectedBaller = 0;
    let selectedBallerId = [];
    this.teamMembers.forEach(member => {
      var isChecked = $('#' + member.userName + '-input');
      if (isChecked[0].ariaChecked) {
        countSelectedBaller++;
        selectedBallerId.push(member.id);
      }
    });

    let model = new PlayerIdsModel();
    model.userId = Number(this.userId);
    model.matchId = Number(this.matchId);

    this.matchService.getBattingTeam(this.matchId)
      .then(teamType => {
        teamType = teamType === GroupType.GroupA ? GroupType.GroupB : GroupType.GroupA;
        model.teamType = Number(teamType);
        model.PlyaerId1 = Number(selectedBallerId[0]);
        model.PlyaerId2 = -1;
        model.PlyaerId3 = -1;
        return this.matchService.updateCurrentBowler(model);
      })
      .then(result => {
        if (result) {
          this.router.navigate(['/scoreboard'],
            { queryParams: { matchId: this.matchId } });
        }
      })

    if (countSelectedBaller === 1 && selectedBallerId.length === 1) {
      console.log( 'Selected Baller : ' , selectedBallerId);
      console.log( 'Counted Baller : ' , countSelectedBaller);
    }
  }



}
