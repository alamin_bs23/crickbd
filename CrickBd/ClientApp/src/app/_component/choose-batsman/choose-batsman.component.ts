import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatchService } from '../../_service/match.service';
import { TeamService } from '../../_service/team.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-choose-batsman',
  templateUrl: './choose-batsman.component.html',
  styleUrls: ['./choose-batsman.component.css']
})
export class ChooseBatsmanComponent implements OnInit {
  matchId;
  teamMembers;
  isChecked;
  onStricker;
  nonStricker;
  teamType;
  outPlayerId;

  constructor(
    private route: ActivatedRoute,
    private matchService: MatchService,
    private teamService: TeamService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route
      .queryParams
      .subscribe(params => {
        this.matchId = Number(params['matchId']);
      });
    this.getTeamMembers();
  }

  getTeamMembers() {
    this.matchService.getBattingTeam(this.matchId)
      .then(teamType => {
        var teamGroup = teamType;
        this.teamType = Number(teamType);
        return this.teamService.getTeamMembers(Number(teamGroup), this.matchId);
      })
      .then(teamMembers => {
        this.teamMembers = teamMembers;
        this.isChecked = true;
        console.log(teamMembers);
        return this.matchService.getCurrentOnStricker(this.matchId, this.teamType);
      })
      .then(response => response.json())
      .then(onStricke => {
        this.onStricker = onStricke;
        console.log('On stricker', onStricke);
        return this.matchService.getCurrentNonStricker(this.matchId, this.teamType);
      })
      .then(response => response.json())
      .then(nonStricker => {
        console.log('Non stricker', nonStricker);
        this.nonStricker = nonStricker;
      })
      .catch(ex => {
        console.log(ex);
      })
  }

  validateChecked() {
    var item = $('.mat-checkbox-checked');
    if (item.length > 1) {
      alert('Please select only one batsman!');
      return false;
    }
    else if (item.length === 0) {
      alert("Opps! You don't select any batsman!");
      return false;
    }
    if (!(this.outPlayerId > 0)) {
      alert("Opps! Please select the name of out player!");
      return false;
    }
    if (!confirm('Are you sure about this action!')) {
      return false;
    }
    return true;
  }

  doCheck(event) {
    if (event.checked) {
      this.isChecked = true;
    }
  };

  goNext() {
    if (!this.validateChecked()) return;
    var selectedBatsman = $('.batsman-choose .mat-checkbox-checked');
    var playerId = selectedBatsman[0].id
    console.log(this.outPlayerId);
    this.matchService.outBatsman(this.matchId, Number(this.outPlayerId))
      .then(isOut => {
        if (isOut) {
          return this.matchService.setBatsman(this.matchId, Number(playerId));
        }
        else {
          return Promise.reject();
        }
      })
      .then(result => {
        if (result === true) {
          this.router.navigate(['/scoreboard'],
            { queryParams: { matchId: this.matchId } });
        }
        else {
          return Promise.reject();
        }
      })
      .catch(ex => {
        console.log(ex);
      });
  }

  batsmanOut(event) {
    var list = $('.serv li span');
    for (var id = 0; id < list.length; id++) {
      list[id].classList.remove('batsman-out');
    }

    var item = event.target;
    item.classList.add('batsman-out');
    this.outPlayerId = Number(item.id);
  }

}
