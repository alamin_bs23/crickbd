import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatchService } from '../../_service/match.service';
import { GroupType, BallStatus } from '../../_model/User';
import { TeamService } from '../../_service/team.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-score-dashboard',
  templateUrl: './score-dashboard.component.html',
  styleUrls: ['./score-dashboard.component.css']
})
export class ScoreDashboardComponent implements OnInit {
  currnetRun;
  currentWicket;
  isOnStricker;
  onStrickerBattingName;
  onStrickerBattingScore;
  onStrickerBallCount;
  nonStrickerBattingName;
  nonStrickerBattingScore;
  nonStrickerBallCount;
  currentBowlerName;
  validBowlerBallCount;
  bowlerGivenRun;
  matchId;
  teamType;
  boldDotBetweenTwoSpeach = "&nbsp;&nbsp;&#8226;&nbsp;&nbsp;";
  previousBallsRecords;
  possibleRun = ["0","1","2","3","4","5","6","7"];
  extra = ["Wide", "No", "Leg by"];
  currentBallStatus;
  recentBalls;
  battingTeamName;
  ballingTeamName;

  ballStatusMapping = {
    'Wide': 3,
    'No': 1,
    'Legby': 5,
    'RightBall': 2
  }
  ballStatusReverse = {
    '3': 'WB',
    '1': 'NB',
    '5': 'LB'
  }

  constructor(
    private route: ActivatedRoute,
    private matchService: MatchService,
    private teamService: TeamService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route
      .queryParams
      .subscribe(params => {
        this.matchId = Number(params['matchId']);
      });

    this.setConfiguration();
  }

  setConfiguration() {
    this.matchService.getBattingTeam(this.matchId)
      .then(teamType => {
        this.teamType = Number(teamType);
        return this.teamService.getTeamScore(this.teamType, this.matchId);
      })
      .then(score => {
        this.currnetRun = Number(score);
        var opponentGroup = this.teamType == GroupType.GroupA ? GroupType.GroupB : GroupType.GroupA;
        return this.matchService.getCurrentBowler(Number(this.matchId), opponentGroup);
      })
      .then(response => response.json())
      .then(currentBowler => {
        this.currentBowlerName = currentBowler.playerName;
        this.validBowlerBallCount = currentBowler.bowlerBallCount;
        this.bowlerGivenRun = currentBowler.bowlerGivenRun;
        this.ballingTeamName = currentBowler.cricketTeams.teamName;
        console.log(currentBowler);
        return this.matchService.getCurrentNonStricker(Number(this.matchId), this.teamType);
      })
      .then(response => response.json())
      .then(currentNonStricker => {
        console.log(currentNonStricker);
        this.nonStrickerBattingName = currentNonStricker.playerName;
        this.nonStrickerBattingScore = currentNonStricker.battingCurrentRun;
        this.nonStrickerBallCount = currentNonStricker.battingBallCount;
        this.battingTeamName = currentNonStricker.cricketTeams.teamName;
        return this.matchService.getCurrentOnStricker(Number(this.matchId), this.teamType);
      })
      .then(response => response.json())
      .then(currentOnStricker => {
        this.onStrickerBattingName = currentOnStricker.playerName;
        this.onStrickerBattingScore = currentOnStricker.battingCurrentRun;
        this.onStrickerBallCount = currentOnStricker.battingBallCount;
        return this.matchService.getRecentBalls(this.matchId);
      })
      .then(response => response.json())
      .then(recentBalls => {
        this.recentBalls = recentBalls;
      })
      .catch(ex => {
        console.log('Exception : ' , ex);
      });
    
  }

  updateRun($el, run) {
    if (!confirm('Are you sure!')) {
      return;
    }
    $('.posible-run').removeClass('selected-on').addClass('selected-off');
    $el.classList.remove('selected-off');
    $el.classList.add('selected-on');
    var ballStatus = this.ballStatusMapping[this.currentBallStatus] || BallStatus.RightBall;
    this.teamService.updateTeamScore(this.teamType, this.matchId, Number(run), Number(ballStatus))
      .then(result => {
        this.setConfiguration();
        this.reConfiguration();
      })
  }

  updateWideClass($el,ballStatus) {
    this.currentBallStatus = ballStatus.replace(' ','');
    $('.ballStatus').removeClass('selected-on').addClass('selected-off');
    $el.classList.remove('selected-off');
    $el.classList.add('selected-on');
  }

  reConfiguration() {
    $('.ballStatus').removeClass('selected-on').addClass('selected-off');
    $('.posible-run').removeClass('selected-on').addClass('selected-off');
    this.currentBallStatus = "RightBall";
  }

  swapStrickerAndNonStricker() {
    this.matchService.swapStrickerAndNonStricker(this.matchId)
      .then(x => {
        console.log(x);
        this.setConfiguration();
      });
  }

  overCompleted() {
    this.router.navigate(['/choosebowller'],
      { queryParams: { matchId: this.matchId } });
  }
  batsmanIsOut() {
    this.router.navigate(['/choosebatsman'],
      { queryParams: { matchId: this.matchId } });
  }
}
