import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { equalValueValidator } from '../../_helpers/equal-value-validator';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../_service/authentication.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  loading = false;
  submitted = false;
  error: any;
  model: any;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }


  buildForm() {
    this.signupForm = this.formBuilder.group({
      UserName: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      email:['',Validators.required]
    });
  }
      
  get f() { return this.signupForm.controls; }

  onSubmit() {
    this.submitted = true;
      // stop here if form is invalid
    if(this.signupForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.signup(this.signupForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['']);
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }
}
