import { Component, OnInit, Input, ViewChild} from '@angular/core';
import { ShareDataService } from '../../_service/share-data.service';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Observable, async } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import * as $ from 'jquery';
import { HttpRequestService } from '../../_service/httpRequest.service';
import { MatchService } from '../../_service/match.service';
import { UserService } from '../../_service/user.service';
import { GroupType } from '../../_model/User';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-team-a',
  templateUrl: './team-a.component.html',
  styleUrls: ['./team-a.component.css']
})
export class TeamAComponent implements OnInit {
  teamForm: FormGroup;
  message = "Hello";
  matchId;
  GroupName = "Team A";
  control = new FormControl();
  submitted = false; loading = false;
  showForm = false;
  disabled: boolean;
  selectedValues: any;
  @Input() optionItems: any[];
  @ViewChild('combo', { static: true }) combo;
  streets: string[] = [];

  constructor(
    private shareDataService: ShareDataService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private httpRequestService: HttpRequestService,
    private matchService: MatchService,
    private userService: UserService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.getUsers();
    this.route
      .queryParams
      .subscribe(params => {
        this.matchId = params['matchId'];
      });
    this.ifCompletedTeamCreationNavigateToTosPage();
    this.showForm = true;
    this.buildForm();
    

    this.shareDataService.currentMessage.subscribe(message => this.message = message);
  }

  buildForm() {
    this.teamForm = this.formBuilder.group({
      matchId: [parseInt(this.matchId)],
      TeamName: ['', Validators.required],
      TeamType: [GroupType.GroupA, Validators.required],
      member1: ['', Validators.required],
      member2: ['', Validators.required],
      member3: ['', Validators.required],
      member4: ['', Validators.required],
      member5: ['', Validators.required],
      member6: ['', Validators.required],
      member7: ['', Validators.required],
      member8: ['', Validators.required],
      member9: ['', Validators.required],
      member10: ['', Validators.required],
      member11: ['', Validators.required]
    });
  }


  promisedParseJSON(json) {
    return new Promise((resolve, reject) => {
      try {
        resolve(JSON.parse(json))
      } catch (e) {
        reject(e)
      }
    });
  };


  private getUsers() {
    if (Boolean(this.optionItems)) return;
    fetch('api/users/all?userName=' + '')
      .then(response => response.json())
      .then(users => {
        console.log(users);
        var user = users.map(x => {
          return { id: x.userName, name: x.userName };
        });
        this.optionItems = user;
      });
  }

  private _normalizeValue(value: string): string {
    return value.toLowerCase().replace(/\s/g, '');
  }

  isAll() {
    return "alamin";
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    console.log('on submit called!');
    console.log(this.teamForm.value);
    var body = $('body');
    console.log(body);

    if (this.teamForm.invalid) {
      //return;
    }

    this.userService.getCurrentUserId()
      .then(userId => {
        return this.matchService.getMatchPlayerCount(this.matchId, userId);
      })
      .then(count => {
        return this.httpRequestService.post("api/team/existing/member", this.teamForm.value)
          .then(existingPlayer => {
            return new Promise(function (resolve, reject) {
              try {
                let isValidExistingPlayer = count === existingPlayer;
                console.log(count, existingPlayer);
                resolve(isValidExistingPlayer);
              } catch (e) {
                reject(e);
              }
            })
          })
      })
      .then(isValid => {
        if (isValid) {
          return this.httpRequestService.get(`api/team/isCreated?teamType=${GroupType.GroupA}&matchId=${this.matchId}`);
        }
        else {
          return Promise.reject();
        }
      })
      .then(isGroupACreated => {
        if (!isGroupACreated) {
          return this.httpRequestService.post("api/team/create/add/member", this.teamForm.value);
        } else {
          return this.httpRequestService.get(`api/team/isCreated?teamType=${GroupType.GroupB}&matchId=${this.matchId}`);
        }
      })
      .then(isGroupBCreated => {
        if (isGroupBCreated === undefined) {
          return Promise.reject();
        }
        if (!isGroupBCreated) {
          this.teamForm.value['TeamType'] = GroupType.GroupB;
          return this.httpRequestService.post("api/team/create/add/member", this.teamForm.value);
        }
        else if (isGroupBCreated === true) {
          return new Promise(function (resolve, reject) {
            try {
              resolve(isGroupBCreated);
            } catch (e) {
              reject(e);
            }
          });
        }
        else {
          this.GroupName = 'Team B';
        }
      })
      .then(completedTeamCreation => {
        if (completedTeamCreation !== undefined) {
          this.router.navigate(['/tos'],
            { queryParams: { matchId: this.matchId } });
        }
        else {
          return Promise.reject();
        }
      })
      .catch(ex => {
        console.log(ex);
      });
  }

  ifCompletedTeamCreationNavigateToTosPage() {
    this.httpRequestService.get(`api/team/isCreated?teamType=${GroupType.GroupA}&matchId=${this.matchId}`)
      .then(isTeam1IsCreated => {
        if (isTeam1IsCreated) {
          return this.httpRequestService.get(`api/team/isCreated?teamType=${GroupType.GroupB}&matchId=${this.matchId}`)
        }
        else {
          return Promise.reject();
        }
      })
      .then(isTeam2IsCreated => {
        if (isTeam2IsCreated) {
          this.router.navigate(['/tos'],
            { queryParams: { matchId: this.matchId } });
        }
        else {
          Promise.reject();
        }
      })
      .catch(e => {
        console.log(e);
      })
  }


  toggleCheckAll(values: any) {
    if (values.currentTarget.checked) {
      this.selectAllItems();
    } else {
      this.unselectAllItems();
    }
  }
  onChange(event) {
    for (let i = 0; i < event.length; i++) {
      this.teamForm.value['member'+(i+1)] = event[i];
    }
    console.log(this.teamForm);
  }

  onTouched() {
    this.getUsers();
    console.log('Tourched!');
  }

  onSelectionChange(selectedItems) {
    if (Array.isArray(selectedItems)) {
      const newList = selectedItems.map((x) => x.id);
      this.selectedValues = [...newList]
      this.onChange([...newList]);
    }
    this.onTouched();
  }

  writeValue(obj: any): void {
    this.combo.select([...obj]);
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  private selectAllItems() {
    const newList = this.optionItems.map((x) => x.id);
    this.selectedValues = [...newList];
    this.onChange([...newList]);
  }

  private unselectAllItems() {
    this.selectedValues = [];
    this.onChange([]);
  }



}
