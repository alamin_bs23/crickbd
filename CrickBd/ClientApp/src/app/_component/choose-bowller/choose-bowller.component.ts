import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatchService } from '../../_service/match.service';
import { TeamService } from '../../_service/team.service';
import { GroupType } from '../../_model/User';
import * as $ from 'jquery';

@Component({
  selector: 'app-choose-bowller',
  templateUrl: './choose-bowller.component.html',
  styleUrls: ['./choose-bowller.component.css']
})
export class ChooseBowllerComponent implements OnInit {
  matchId;
  teamMembers;
  checkedBowlerId;
  isChecked = false;
  selectedOutPlayer;

  constructor(
    private route: ActivatedRoute,
    private matchService: MatchService,
    private teamService: TeamService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route
      .queryParams
      .subscribe(params => {
        this.matchId = Number(params['matchId']);
      });
    this.getTeamMembers();
  }
  getTeamMembers() {
    this.matchService.getBattingTeam(this.matchId)
      .then(teamType => {
        var teamGroup = teamType == GroupType.GroupA ? GroupType.GroupB : GroupType.GroupA;
        return this.teamService.getTeamMembers(Number(teamGroup), this.matchId);
      })
      .then(teamMembers => {
        this.teamMembers = teamMembers;
        this.isChecked = true;
        console.log(teamMembers);
      })
      .catch(ex => {
        console.log(ex);
      })
  }

  validateChecked() {
    var item = $('.mat-checkbox-checked');
    if (item.length > 1) {
      alert('Please Select Only one Bowler!');
      return false;
    }
    else if (item.length === 0) {
      alert("Opps! You don't select the bowler!");
      return false;
    }
    if (!confirm('Are you sure about this action!')) {
      return false;
    }
    return true;
  }

  doCheck(event) {
    if (event.checked) {
      this.isChecked = true;
    }
  };

  goNext() {
    if (!this.validateChecked()) return;
    var selectedBowler = $('.bowling-choose .mat-checkbox-checked');
    var playerId = selectedBowler[0].id;
    this.matchService.setBowler(this.matchId, Number(playerId))
      .then(result => {
        if (result === true) {
          this.router.navigate(['/scoreboard'],
            { queryParams: { matchId: this.matchId } });
        }
        else {
          return Promise.reject();
        }
      })
      .catch(ex => {
        console.log(ex);
      });
  }

}
