import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GetCrickInfoService {
  private baseUrl;
  headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });
  options = {
    headers: this.headers
  }
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  getInfo() {
    return this.http.get('weather/1/information');
  }
  runPost(categoryName: string) {
    let contact = JSON.stringify({ Avarage: 43.5, BattingName:'Alamin Hossain' });

    return this.http.post(this.baseUrl + 'weather/1/run', contact, this.options)
      .subscribe(value => console.log(value));
  }
  createMatch(match) {
    return this.http.post(this.baseUrl + 'api/cricket/match/create', match, this.options).toPromise();
  }
}
