import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GetCrickInfoService } from '../get-crick-info.service';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
  public forecasts;

  constructor(private getCrickInfoService: GetCrickInfoService) {
    getCrickInfoService.getInfo().subscribe(result => {
      this.forecasts = result;
    }, error => console.error(error));
  }

}

