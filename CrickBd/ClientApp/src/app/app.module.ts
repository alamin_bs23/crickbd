import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { CricketComponent } from './cricket/cricket.component';
import { MatchCreateComponent } from './match-create/match-create.component';
import { LoginComponent } from './_component/login/login.component';
import { AuthGuardGuard } from './_helpers/auth-guard.guard';
import { SignupComponent } from './_component/signup/signup.component';
import { MatchesComponent } from './_component/matches/matches.component';
import { TeamAComponent } from './_component/team-a/team-a.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { TossComponent } from './_component/toss/toss.component';
import { ScoreDashboardComponent } from './_component/score-dashboard/score-dashboard.component';
import { ChooseBowllerComponent } from './_component/choose-bowller/choose-bowller.component';
import { ChooseBatsmanComponent } from './_component/choose-batsman/choose-batsman.component';
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    CricketComponent,
    MatchCreateComponent,
    LoginComponent,
    SignupComponent,
    MatchesComponent,
    TeamAComponent,
    TossComponent,
    ScoreDashboardComponent,
    ChooseBowllerComponent,
    ChooseBatsmanComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    NgSelectModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, canActivate: [AuthGuardGuard] },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'cricket', component: CricketComponent },
      { path: 'login', component: LoginComponent },
      { path: 'signup', component: SignupComponent },
      { path: 'matches', component: MatchesComponent },
      { path: 'teamA', component: TeamAComponent },
      { path: 'tos', component: TossComponent },
      { path: 'scoreboard', component: ScoreDashboardComponent},
      { path: 'choosebowller', component: ChooseBowllerComponent },
      { path: 'choosebatsman', component: ChooseBatsmanComponent},
      { path: '**', redirectTo: '' }
    ], { relativeLinkResolution: 'legacy' }),

    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
