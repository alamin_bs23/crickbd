import { Component, OnInit } from '@angular/core';
import { GetCrickInfoService } from '../get-crick-info.service';

@Component({
  selector: 'app-cricket',
  templateUrl: './cricket.component.html',
  styleUrls: ['./cricket.component.css']
})
export class CricketComponent implements OnInit {
  public test;
  constructor(private getCrickInfoService: GetCrickInfoService) { }

  ngOnInit(): void {
    this.test = "hello test";
  }
  runPost(run) {
    this.getCrickInfoService.runPost('test');
    console.log(run);
  }
}
