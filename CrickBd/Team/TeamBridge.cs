﻿using CrickBd.Domain;
using CrickBd.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Team
{
    public abstract class TeamBridge
    {
        protected ITeam team;
        public TeamBridge(ITeam team)
        {
            this.team = team;
        }
        public abstract Task InsertTeamMemberAsync(Domain.Player player);
        public abstract Task<IList<Domain.Player>> GetTeamMembers();
        public abstract Task<int> GetTeamScore();
        public abstract Task CreateTeamAsync(CricketTeam team);
        public abstract Task<int> InsertTeamMemberByGroupAsync(TeamCreationModel model);
        public abstract Task<int> IsExistAllMemberByUserNameAsync(TeamCreationModel model);
        public abstract Task<bool> IsTeamCreatedAsync();
        public abstract Task<PlayerInMatch> GetNonStricker();
        public abstract Task<PlayerInMatch> GetOnStricker();
        public abstract Task<PlayerInMatch> GetCurrentBowler();
        public abstract Task<int> UpdateTeamScore(int run ,int ballStatus);
        public abstract Task<IList<TeamBallsRecords>> GetTeamBallsRecords();
        public abstract Task<bool> SwapOnStrickerAndNonStricker();
        public abstract Task<bool> SetBowlerInBowlingTeam(int playerId);
        public abstract Task<bool> SetBatsManInBattingTeam(int playerId);
        public abstract Task<bool> OutBatsManInBattingTeam(int playerId);
    }
}
