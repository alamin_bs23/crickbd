﻿using CrickBd.Service.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.Team
{
    public class TeamCreate
    {
        public static ITeam GetTeam(int teamType, int matchId)
        {
            if (teamType == (int)TeamGroup.GroupA)
            {
                return new TeamA(matchId);
            }
            else if (teamType == (int)TeamGroup.GroupB)
            {
                return new TeamB(matchId);
            }
            else return null;
        }
    }
}
