﻿using CrickBd.Domain;
using CrickBd.Service.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using CrickBd.Service.CricketsTeam;
using CrickBd.Service;
using CrickBd.Service.BallRecord;

namespace CrickBd.Team
{
    public class TeamB : ITeam
    {
        private readonly int matchId;
        public TeamB(int matchId)
        {
            this.matchId = matchId;
        }

        public async Task<CricketTeam> CreateTeamAsync(CricketTeam team)
        {
            using (var serviceScope = ServiceActivator.GetScope())
            {
                ITeamService teamService = serviceScope.ServiceProvider.GetService<ITeamService>();
                team = await teamService.CreateOrUpdateTeamAsync(team);
                return team;
            }
        }

        public async Task<CricketTeam> GetTeamByMatchIdAsync()
        {
            using (var serviceScope = ServiceActivator.GetScope())
            {
                ITeamService teamService = serviceScope.ServiceProvider.GetService<ITeamService>();
                CricketTeam cricketTeam = await teamService.GetCricketTeamByMatchIdWithTeamTypeAsync(this.matchId, (int)TeamGroup.GroupB);
                return cricketTeam;
            }
        }


        public async Task<int> GetTeamScore()
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    ITeamService teamService = serviceScope.ServiceProvider.GetService<ITeamService>();
                    int totalRun = await teamService.GetTeamScore(this.matchId, (int)TeamGroup.GroupB);
                    return totalRun;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task InsertTeamMemberAsync(Domain.Player player)
        {
            try
            {
                CricketTeam cricketTeam = await GetTeamByMatchIdAsync();
                if (cricketTeam == null)
                {
                    throw new NullReferenceException(nameof(cricketTeam));
                }
                PlayerInMatch playerInMatch = new PlayerInMatch
                {
                    MatchId = cricketTeam.matchId,
                    CricketTeamId = cricketTeam.Id,
                    PlayerId = player.Id,
                    PlayerName = player.FullName,
                    BattingCurrentRun = 0,
                    BattingBallCount = 0
                };
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    ITeamService teamService = serviceScope.ServiceProvider.GetService<ITeamService>();
                    await teamService.InsertTeamMemberAsync(playerInMatch);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> IsTeamCreatedAsync()
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    ITeamService teamService = serviceScope.ServiceProvider.GetService<ITeamService>();
                    bool isExistTeam = await teamService.IsTeamCreatedByTeamTypeAndMatchIdAsync((int)TeamGroup.GroupB, this.matchId);
                    return isExistTeam;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<IList<Domain.Player>> GetTeamMembers()
        {
            using (var serviceScope = ServiceActivator.GetScope())
            {
                ITeamService teamService = serviceScope.ServiceProvider.GetService<ITeamService>();
                List<Domain.Player> players = await teamService.GetTeamMemberAsync(this.matchId, (int)TeamGroup.GroupB) as List<Domain.Player>;
                return players;
            }
        }

        public async Task<PlayerInMatch> GetCurrentBowler()
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    ICricketTeamService cricketTeamService = serviceScope.ServiceProvider.GetService<ICricketTeamService>();
                    CricketTeam cricketTeam = await cricketTeamService.GetCricketTeamByMatchIdAndTeamTypeAsync(this.matchId, (int)TeamGroup.GroupB);
                    IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                    PlayerInMatch playerInMatch = await matchService.GetCurrentBowlerByMatchIdAndCricketTeamId(this.matchId, cricketTeam.Id);
                    return playerInMatch;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<PlayerInMatch> GetOnStricker()
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    ICricketTeamService cricketTeamService = serviceScope.ServiceProvider.GetService<ICricketTeamService>();
                    CricketTeam cricketTeam = await cricketTeamService.GetCricketTeamByMatchIdAndTeamTypeAsync(this.matchId, (int)TeamGroup.GroupB);
                    IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                    PlayerInMatch playerInMatch = await matchService.GetOnStrickerByMatchIdAndCricketTeamId(this.matchId, cricketTeam.Id);
                    return playerInMatch;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<PlayerInMatch> GetNonStricker()
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    ICricketTeamService cricketTeamService = serviceScope.ServiceProvider.GetService<ICricketTeamService>();
                    CricketTeam cricketTeam = await cricketTeamService.GetCricketTeamByMatchIdAndTeamTypeAsync(this.matchId, (int)TeamGroup.GroupB);
                    IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                    PlayerInMatch playerInMatch = await matchService.GetNonStrickerByMatchIdAndCricketTeamId(this.matchId, cricketTeam.Id);
                    return playerInMatch;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<int> UpdateTeamScore(int run , int ballStatus)
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    ITeamService teamService = serviceScope.ServiceProvider.GetService<ITeamService>();
                    int totalRun = await teamService.UpdateTeamScore(this.matchId, (int)TeamGroup.GroupB, run , ballStatus);
                    return totalRun;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<IList<TeamBallsRecords>> GetTeamBallsRecords()
        {
            using (var serviceScope = ServiceActivator.GetScope())
            {
                IBallRecordService ballRecordService = serviceScope.ServiceProvider.GetService<IBallRecordService>();
                ICricketTeamService cricketTeamService = serviceScope.ServiceProvider.GetService<ICricketTeamService>();
                CricketTeam cricketTeam = await cricketTeamService.GetCricketTeamByMatchIdAndTeamTypeAsync(this.matchId, (int)TeamGroup.GroupB);
                IList<TeamBallsRecords> teamBallsRecords = await ballRecordService.GetTeamBallsRecordsByCricketTeamId(cricketTeam.Id);
                return teamBallsRecords;
            }
        }
        public async Task<bool> SwapOnStrickerAndNonStricker()
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    PlayerInMatch onStrikcer = await GetOnStricker();
                    PlayerInMatch nonStricker = await GetNonStricker();
                    IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                    bool result = await matchService.SwapOnStrickerAndNonStricker(onStrikcer, nonStricker);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> SetBowlerInBowlingTeam(int playerId) 
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                    await matchService.SetBowlerInBallingTeam(this.matchId, playerId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> SetBatsManInBattingTeam(int playerId)
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                    var result = await matchService.SetBatsmanInBattingTeam(this.matchId, playerId);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> OutBatsManInBattingTeam(int playerId)
        {
            try
            {
                using (var serviceScopre = ServiceActivator.GetScope())
                {
                    IMatchService matchService = serviceScopre.ServiceProvider.GetService<IMatchService>();
                    var result = await matchService.OutBatsManInBattingTeam(this.matchId, playerId);
                    return result;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
