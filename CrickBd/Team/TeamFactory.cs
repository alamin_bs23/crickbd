﻿using CrickBd.Domain;
using CrickBd.Model;
using CrickBd.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using CrickBd.Service.CricketsTeam;

namespace CrickBd.Team
{
    public class TeamFactory : TeamBridge
    {
        public TeamFactory(ITeam team) : base(team)
        {

        }

        public override Task CreateTeamAsync(CricketTeam team)
        {
            try
            {
                return this.team.CreateTeamAsync(team);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public override async Task<PlayerInMatch> GetCurrentBowler()
        {
            try
            {
                return await team.GetCurrentBowler();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public override async Task<PlayerInMatch> GetNonStricker()
        {
            try
            {
                return await team.GetNonStricker();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public override async Task<PlayerInMatch> GetOnStricker()
        {
            try
            {
                return await team.GetOnStricker();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public override async Task<IList<TeamBallsRecords>> GetTeamBallsRecords()
        {
            return await team.GetTeamBallsRecords();
        }

        public override Task<IList<Domain.Player>> GetTeamMembers()
        {
            try
            {
                return this.team.GetTeamMembers();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public override async Task<int> GetTeamScore()
        {
            return await team.GetTeamScore();
        }

        public override async Task InsertTeamMemberAsync(Domain.Player player)
        {
            try
            {
                await team.InsertTeamMemberAsync(player);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public override async Task<int> InsertTeamMemberByGroupAsync(TeamCreationModel model)
        {
            try
            {
                int addedMember = 0;
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    IUserService userService = serviceScope.ServiceProvider.GetService<IUserService>();
                    Type myType = typeof(TeamCreationModel);
                    IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
                    foreach(PropertyInfo prop in props)
                    {
                        if (prop.ToString().Contains("member"))
                        {
                            var propValue = prop.GetValue(model);
                            var player = await userService.GetByUserNameAsync(propValue.ToString());
                            if (player == null) continue;
                            await InsertTeamMemberAsync(player);
                            addedMember++;
                        }
                    }
                }
                return addedMember;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public override async Task<int> IsExistAllMemberByUserNameAsync(TeamCreationModel model)
        {
            try
            {
                int existingPlayer = 0;
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    IUserService userService = serviceScope.ServiceProvider.GetService<IUserService>();
                    Type myType = typeof(TeamCreationModel);
                    IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());
                    foreach (PropertyInfo prop in props)
                    {
                        if (prop.ToString().Contains("member"))
                        {
                            var propValue = prop.GetValue(model);
                            var player = await userService.GetByUserNameAsync(propValue.ToString());
                            if(player != null)
                            {
                                existingPlayer++;
                            }
                        }
                    }
                }
                return existingPlayer;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public override async Task<bool> IsTeamCreatedAsync()
        {
            try
            {
                return await team.IsTeamCreatedAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public override async Task<bool> OutBatsManInBattingTeam(int playerId)
        {
            try
            {
                return await team.OutBatsManInBattingTeam(playerId);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public override Task<bool> SetBatsManInBattingTeam(int playerId)
        {
            return team.SetBatsManInBattingTeam(playerId);
        }

        public override Task<bool> SetBowlerInBowlingTeam(int playerId)
        {
            return team.SetBowlerInBowlingTeam(playerId);
        }

        public override async Task<bool> SwapOnStrickerAndNonStricker()
        {
            return await team.SwapOnStrickerAndNonStricker();
        }

        public override async Task<int> UpdateTeamScore(int run, int ballStatus)
        {
            return await team.UpdateTeamScore(run, ballStatus);
        }
    }
}
