﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrickBd.Domain;

namespace CrickBd.Team
{
    public interface ITeam
    {
        public Task InsertTeamMemberAsync(Domain.Player player);
        public Task<IList<Domain.Player>> GetTeamMembers();
        public Task<int> GetTeamScore();
        public Task<CricketTeam> CreateTeamAsync(CricketTeam team);
        public Task<CricketTeam> GetTeamByMatchIdAsync();
        public Task<bool> IsTeamCreatedAsync();
        public Task<PlayerInMatch> GetCurrentBowler();
        public Task<PlayerInMatch> GetOnStricker(); 
        public Task<PlayerInMatch> GetNonStricker();
        public Task<int> UpdateTeamScore(int run ,int ballStatus);
        public Task<IList<TeamBallsRecords>> GetTeamBallsRecords();
        public Task<bool> SwapOnStrickerAndNonStricker();
        public Task<bool> SetBowlerInBowlingTeam(int playerId);
        public Task<bool> SetBatsManInBattingTeam(int playerId);
        public Task<bool> OutBatsManInBattingTeam(int playerId);
    } 
}
