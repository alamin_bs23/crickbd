﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd
{
    public enum TeamGroup
    {
        GroupA = 0,
        GroupB = 1
    };

    public enum BallStatus
    {
        NoBall = 1,
        RightBall = 2,
        WideBall = 3, 
        DadeBall = 4,
        LegBy = 5
    };

    public static class SwapEnum
    {
        public static TeamGroup swapTeamGroup(TeamGroup teamGroup)
        {
            teamGroup = teamGroup == TeamGroup.GroupA ? TeamGroup.GroupB : TeamGroup.GroupA;
            return teamGroup;
        }
    }

}
