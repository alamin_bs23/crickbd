﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CrickBd.EventHandler
{
    public class RunEventHandles
    {
        public event EventHandler<RunDomainEvent> RunDomainEvent; 

        public void startProcess(RunDomainEvent runDomainEvent) 
        {
            OnRaiseTheEvent(runDomainEvent); 
        }

        public Task OnRaiseTheEvent(RunDomainEvent runDomainEvent) 
        {
            RunDomainEvent?.Invoke(this, runDomainEvent);
            return Task.CompletedTask;
        }
    }
}
