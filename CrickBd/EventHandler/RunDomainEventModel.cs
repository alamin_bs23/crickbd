﻿using CrickBd.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.EventHandler
{
    public class RunDomainEvent : EventArgs
    {
        public CricketTeam cricketTeam { get; }
        public int run { get; }
        public int matchId { get; }
        public int teamType { get; }
        public int ballStatus { get; }
        public RunDomainEvent 
            (
            CricketTeam cricketTeam,
            int run,
            int matchId,
            int teamType,
            int ballStatus
            )
        {
            this.cricketTeam = cricketTeam;
            this.run = run;
            this.matchId = matchId;
            this.teamType = teamType;
            this.ballStatus = ballStatus;
        }
    }
}
