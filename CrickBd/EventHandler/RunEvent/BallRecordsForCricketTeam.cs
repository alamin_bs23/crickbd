﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrickBd.Domain;
using CrickBd.Service;
using CrickBd.Service.BallRecord;
using Microsoft.Extensions.DependencyInjection;

namespace CrickBd.EventHandler.RunEvent
{
    public class BallRecordsForCricketTeam : RunDomainEventRaiser
    {
        public BallRecordsForCricketTeam(RunEventHandles runEventHandles) : base(runEventHandles)
        {
        }

        public override void RunEventHandles_RunDomainEvent(object sender, RunDomainEvent @event)
        {
            using (var serviceScope = ServiceActivator.GetScope())
            {
                BallStatus ballStatus = (BallStatus)@event.ballStatus;
                IBallRecordService ballRecordService = serviceScope.ServiceProvider.GetService<IBallRecordService>();
                TeamBallsRecords teamBallsRecords = new TeamBallsRecords
                {
                    CricketTeamId = @event.cricketTeam.Id,
                    BallStatus = (int)ballStatus,
                    Run = @event.run
                };
                ballRecordService.insertTeamBallsRecordAsync(teamBallsRecords);
            }
        }
    }
}
