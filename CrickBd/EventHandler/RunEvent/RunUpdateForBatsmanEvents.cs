﻿using CrickBd.Service;
using CrickBd.Service.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using CrickBd.Domain;

namespace CrickBd.EventHandler.RunEvent
{
    public class RunUpdateForBatsmanEvent : RunDomainEventRaiser
    {
        public RunUpdateForBatsmanEvent(RunEventHandles runEventHandles) : base(runEventHandles)
        {
        }

        public override void RunEventHandles_RunDomainEvent(object sender, RunDomainEvent @event)
        {
            using (var serviceScope = ServiceActivator.GetScope())
            {
                BallStatus ballStatus = (BallStatus)(@event.ballStatus);
                if (ballStatus == BallStatus.LegBy || ballStatus == BallStatus.WideBall
                    || ballStatus == BallStatus.DadeBall)
                {
                    return;
                }
                IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                ITeamService teamService = serviceScope.ServiceProvider.GetService<ITeamService>();
                PlayerInMatch currentBatsMan = matchService.GetOnStrickerByMatchIdAndCricketTeamId(@event.cricketTeam.matchId, @event.cricketTeam.Id).Result;
                currentBatsMan.BattingCurrentRun += (@event.run - (ballStatus == BallStatus.NoBall ? 1 : 0));
                currentBatsMan.BattingBallCount += 1;
                matchService.ApplyChanges();
            }
        }
    }


    public class InvokeRunUpdateEvent
    {
        public static Task Invoke(RunDomainEvent @event)
        {
            RunEventHandles runEventHandles = new RunEventHandles();
            new RunUpdateForBatsmanEvent(runEventHandles);
            new RunUpdateForBowlerEvent(runEventHandles);
            new BallRecordsForCricketTeam(runEventHandles);
            new RunUpdateForMatchOverEvent(runEventHandles);
            runEventHandles.startProcess(@event);
            return Task.CompletedTask; 
        }
    }
}
