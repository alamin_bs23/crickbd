﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrickBd.Domain;
using CrickBd.Service;
using CrickBd.Service.MatchOverEvent;
using CrickBd.Service.Team;
using Microsoft.Extensions.DependencyInjection;

namespace CrickBd.EventHandler.RunEvent
{
    public class RunUpdateForMatchOverEvent : RunDomainEventRaiser
    {
        public RunUpdateForMatchOverEvent(RunEventHandles runEventHandles) : base(runEventHandles)
        {
        }

        public override void RunEventHandles_RunDomainEvent(object sender, RunDomainEvent @event)
        {
            try
            {
                using (var serviceScope = ServiceActivator.GetScope())
                {
                    BallStatus ballStatus = (BallStatus)@event.ballStatus;
                    IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                    ITeamService teamService = serviceScope.ServiceProvider.GetService<ITeamService>();
                    IMatchOverService matchOverService = serviceScope.ServiceProvider.GetService<IMatchOverService>();
                    TeamGroup teamGroup = SwapEnum.swapTeamGroup((TeamGroup)@event.teamType);
                    CricketTeam bowlerCricketTeam = teamService.GetCricketTeamByMatchIdWithTeamTypeAsync(@event.cricketTeam.matchId, (int)teamGroup).Result;
                    PlayerInMatch currentBowler = matchService.GetCurrentBowlerByMatchIdAndCricketTeamId(@event.cricketTeam.matchId, bowlerCricketTeam.Id).Result;

                    MatchOver matchOver1 = matchOverService.GetMatchOver(bowlerCricketTeam.Id, currentBowler.PlayerId);
                    if(matchOver1 == null)
                    {
                        MatchOver matchOver = new MatchOver
                        {
                            TotalRun = 0,
                            PlayerId = currentBowler.PlayerId,
                            MatchId = @event.matchId,
                            CricketTeamId = bowlerCricketTeam.Id,
                            IsFinished = false
                        };
                        matchOverService.InsertMatchOver(matchOver);
                        matchOverService.UpdateMatchOver(matchOver, @event.run);
                    }
                    else
                    {
                        matchOverService.UpdateMatchOver(matchOver1, @event.run);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
