﻿using System;
using CrickBd.Domain;
using CrickBd.Service;
using CrickBd.Service.Team;
using Microsoft.Extensions.DependencyInjection;


namespace CrickBd.EventHandler.RunEvent
{
    public class RunUpdateForBowlerEvent : RunDomainEventRaiser
    {
        public RunUpdateForBowlerEvent(RunEventHandles runEventHandles) : base(runEventHandles)
        {
        }

        public override void RunEventHandles_RunDomainEvent(object sender, RunDomainEvent @event)
        {
            using (var serviceScope = ServiceActivator.GetScope())
            {
                BallStatus ballStatus = (BallStatus)@event.ballStatus;
                IMatchService matchService = serviceScope.ServiceProvider.GetService<IMatchService>();
                ITeamService teamService = serviceScope.ServiceProvider.GetService<ITeamService>();
                TeamGroup teamGroup = SwapEnum.swapTeamGroup((TeamGroup)@event.teamType);
                CricketTeam bowlerCricketTeam = teamService.GetCricketTeamByMatchIdWithTeamTypeAsync(@event.cricketTeam.matchId, (int)teamGroup).Result;
                PlayerInMatch currentBowler = matchService.GetCurrentBowlerByMatchIdAndCricketTeamId(@event.cricketTeam.matchId, bowlerCricketTeam.Id).Result;
                currentBowler.BowlerBallCount += (ballStatus == BallStatus.RightBall ? 1 : 0);
                currentBowler.BowlerGivenRun += @event.run;
                matchService.ApplyChanges();
            }
        }
    }
}
