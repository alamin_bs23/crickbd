﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd.EventHandler
{
    public abstract class RunDomainEventRaiser
    {
        protected RunEventHandles runEventHandles;
        public RunDomainEventRaiser(RunEventHandles runEventHandles)
        {
            this.runEventHandles = runEventHandles;
            this.registrationEvent();
        }
        private void registrationEvent()
        {
            this.runEventHandles.RunDomainEvent += RunEventHandles_RunDomainEvent;
        }

        public abstract void RunEventHandles_RunDomainEvent(object sender, RunDomainEvent @event);
    }
}
