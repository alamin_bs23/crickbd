﻿using CrickBd.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CrickBd.Data
{
    public class CricketDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=SQL5101.site4now.net;Initial Catalog=DB_A6E532_helAlamin;User Id=DB_A6E532_helAlamin_admin;Password=Alamin1@1");
        }
        public DbSet<Domain.Player> Users { get; set; } 
        public DbSet<Batting> Battings { get; set; } 
        public DbSet<Match> Matchs { get; set; } 
        public DbSet<CricketTeam> CricketTeams { get; set; } 
        public DbSet<PlayerInMatch> playerInMatches { get; set; } 
        public DbSet<MatchOver> MatchOvers { get; set; } 
        public DbSet<Ball> Balls { get; set; }  
        public DbSet<TeamBallsRecords> TeamBallsRecords { get; set; }  
    } 
}
