﻿using AutoMapper;
using CrickBd.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrickBd
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<Model.Player, Domain.Player>(MemberList.Source);
            CreateMap<Model.CricketTeamModel, Domain.CricketTeam>(MemberList.Source);
            CreateMap<TeamCreationModel, Domain.CricketTeam>(MemberList.Source);
        }
    }
}
